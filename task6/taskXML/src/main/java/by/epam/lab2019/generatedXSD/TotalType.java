
package by.epam.lab2019.generatedXSD;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TotalType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TotalType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Base" type="{}BaseType" minOccurs="0"/>
 *         &lt;element name="Taxes" type="{}TaxesType" minOccurs="0"/>
 *         &lt;element name="Fees" type="{}FeesType" minOccurs="0"/>
 *         &lt;element name="Total" type="{}TotalType" minOccurs="0"/>
 *         &lt;element name="Extension" type="{}ExtensionType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TotalType", propOrder = {
        "content"
})
public class TotalType {

    @XmlElementRefs({
            @XmlElementRef(name = "Fees", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Taxes", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Base", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Extension", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "Total", type = JAXBElement.class, required = false)
    })
    @XmlMixed
    protected List<Serializable> content;
    @XmlAttribute(name = "Amount")
    protected String amount;
    @XmlAttribute(name = "CurrencyCode")
    protected String currencyCode;

    public TotalType() {
        this.content = new ArrayList<>();
        this.amount = "163.52";
        this.currencyCode = "EUR";
    }

    /**
     * Gets the value of the content property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the content property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContent().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * {@link JAXBElement }{@code <}{@link FeesType }{@code >}
     * {@link JAXBElement }{@code <}{@link TaxesType }{@code >}
     * {@link JAXBElement }{@code <}{@link ExtensionType }{@code >}
     * {@link JAXBElement }{@code <}{@link BaseType }{@code >}
     * {@link JAXBElement }{@code <}{@link TotalType }{@code >}
     */
    public List<Serializable> getContent() {
        if (content == null) {
            content = new ArrayList<Serializable>();
        }
        return this.content;
    }

    /**
     * Gets the value of the amount property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

}
