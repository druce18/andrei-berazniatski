
package by.epam.lab2019.generatedXSD;

import com.sun.xml.internal.ws.api.model.ExceptionType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AirType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AirType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Options" type="{}OptionsType"/>
 *         &lt;element name="Extension" type="{}ExtensionType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Direction" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TotalTravelTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirType", propOrder = {
    "options",
    "extension"
})
public class AirType {

    @XmlElement(name = "Options", required = true)
    protected OptionsType options;
    @XmlElement(name = "Extension", required = true)
    protected ExtensionType extension;
    @XmlAttribute(name = "Direction")
    protected String direction;
    @XmlAttribute(name = "TotalTravelTime")
    protected String totalTravelTime;

    public AirType() {
        this.options = new OptionsType();
        this.extension = new ExtensionType();
        this.direction = "OneWay";
        this.totalTravelTime = "PT2H55M";
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nAirType{");
        sb.append(" direction='").append(direction).append('\'');
        sb.append(", totalTravelTime='").append(totalTravelTime).append('\'');
        sb.append("}\n");
        return sb.toString();
    }

    /**
     * Gets the value of the options property.
     * 
     * @return
     *     possible object is
     *     {@link OptionsType }
     *     
     */
    public OptionsType getOptions() {
        return options;
    }

    /**
     * Sets the value of the options property.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsType }
     *     
     */
    public void setOptions(OptionsType value) {
        this.options = value;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionType }
     *     
     */
    public ExtensionType getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionType }
     *     
     */
    public void setExtension(ExtensionType value) {
        this.extension = value;
    }

    /**
     * Gets the value of the direction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDirection() {
        return direction;
    }

    /**
     * Sets the value of the direction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDirection(String value) {
        this.direction = value;
    }

    /**
     * Gets the value of the totalTravelTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalTravelTime() {
        return totalTravelTime;
    }

    /**
     * Sets the value of the totalTravelTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalTravelTime(String value) {
        this.totalTravelTime = value;
    }

}
