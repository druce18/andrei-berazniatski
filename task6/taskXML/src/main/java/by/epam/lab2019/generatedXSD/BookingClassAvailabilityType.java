
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for BookingClassAvailabilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookingClassAvailabilityType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="BookCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="BookDesig" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Cabin" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingClassAvailabilityType", propOrder = {
    "value"
})
public class BookingClassAvailabilityType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "BookCode")
    protected String bookCode;
    @XmlAttribute(name = "BookDesig")
    protected String bookDesig;
    @XmlAttribute(name = "Cabin")
    protected String cabin;

    public BookingClassAvailabilityType() {
        this.bookCode = "K";
        this.bookDesig = "9";
        this.cabin = "Economy";
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the bookCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookCode() {
        return bookCode;
    }

    /**
     * Sets the value of the bookCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookCode(String value) {
        this.bookCode = value;
    }

    /**
     * Gets the value of the bookDesig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookDesig() {
        return bookDesig;
    }

    /**
     * Sets the value of the bookDesig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookDesig(String value) {
        this.bookDesig = value;
    }

    /**
     * Gets the value of the cabin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCabin() {
        return cabin;
    }

    /**
     * Sets the value of the cabin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCabin(String value) {
        this.cabin = value;
    }

}
