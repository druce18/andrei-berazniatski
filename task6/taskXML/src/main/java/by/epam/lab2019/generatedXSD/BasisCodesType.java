
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BasisCodesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BasisCodesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BasisCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BasisCodesType", propOrder = {
    "basisCode"
})
public class BasisCodesType {

    @XmlElement(name = "BasisCode", required = true)
    protected String basisCode;

    public BasisCodesType() {
        this.basisCode = "322";
    }

    /**
     * Gets the value of the basisCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasisCode() {
        return basisCode;
    }

    /**
     * Sets the value of the basisCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasisCode(String value) {
        this.basisCode = value;
    }

}
