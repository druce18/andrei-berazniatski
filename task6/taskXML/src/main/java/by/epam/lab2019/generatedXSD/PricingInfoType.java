
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Total" type="{}TotalType"/>
 *         &lt;element name="PTCs" type="{}PTCsType"/>
 *         &lt;element name="FareInfos" type="{}FareInfosType"/>
 *         &lt;element name="Extension" type="{}ExtensionType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Source" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricingInfoType", propOrder = {
    "total",
    "ptCs",
    "fareInfos",
    "extension"
})
public class PricingInfoType {

    @XmlElement(name = "Total", required = true)
    protected TotalType total;
    @XmlElement(name = "PTCs", required = true)
    protected PTCsType ptCs;
    @XmlElement(name = "FareInfos", required = true)
    protected FareInfosType fareInfos;
    @XmlElement(name = "Extension", required = true)
    protected ExtensionType extension;
    @XmlAttribute(name = "Source")
    protected String source;

    public PricingInfoType() {
        this.total = new TotalType();
        this.ptCs = new PTCsType();
        this.fareInfos = new FareInfosType();
        this.extension = new ExtensionType();
        this.source = "GDS";
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link TotalType }
     *     
     */
    public TotalType getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalType }
     *     
     */
    public void setTotal(TotalType value) {
        this.total = value;
    }

    /**
     * Gets the value of the ptCs property.
     * 
     * @return
     *     possible object is
     *     {@link PTCsType }
     *     
     */
    public PTCsType getPTCs() {
        return ptCs;
    }

    /**
     * Sets the value of the ptCs property.
     * 
     * @param value
     *     allowed object is
     *     {@link PTCsType }
     *     
     */
    public void setPTCs(PTCsType value) {
        this.ptCs = value;
    }

    /**
     * Gets the value of the fareInfos property.
     * 
     * @return
     *     possible object is
     *     {@link FareInfosType }
     *     
     */
    public FareInfosType getFareInfos() {
        return fareInfos;
    }

    /**
     * Sets the value of the fareInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link FareInfosType }
     *     
     */
    public void setFareInfos(FareInfosType value) {
        this.fareInfos = value;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionType }
     *     
     */
    public ExtensionType getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionType }
     *     
     */
    public void setExtension(ExtensionType value) {
        this.extension = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

}
