
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PTCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PTCType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PassengerQuantity" type="{}PassengerQuantityType"/>
 *         &lt;element name="BasisCodes" type="{}BasisCodesType"/>
 *         &lt;element name="PassengerFare" type="{}PassengerFareType"/>
 *         &lt;element name="Extension" type="{}ExtensionType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Source" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PTCType", propOrder = {
    "passengerQuantity",
    "basisCodes",
    "passengerFare",
    "extension"
})
public class PTCType {

    @XmlElement(name = "PassengerQuantity", required = true)
    protected PassengerQuantityType passengerQuantity;
    @XmlElement(name = "BasisCodes", required = true)
    protected BasisCodesType basisCodes;
    @XmlElement(name = "PassengerFare", required = true)
    protected PassengerFareType passengerFare;
    @XmlElement(name = "Extension", required = true)
    protected ExtensionType extension;
    @XmlAttribute(name = "Source")
    protected String source;

    public PTCType() {
        this.passengerQuantity = new PassengerQuantityType();
        this.basisCodes = new BasisCodesType();
        this.passengerFare = new PassengerFareType();
        this.extension = new ExtensionType();
        this.source = "GDS";
    }

    /**
     * Gets the value of the passengerQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link PassengerQuantityType }
     *     
     */
    public PassengerQuantityType getPassengerQuantity() {
        return passengerQuantity;
    }

    /**
     * Sets the value of the passengerQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengerQuantityType }
     *     
     */
    public void setPassengerQuantity(PassengerQuantityType value) {
        this.passengerQuantity = value;
    }

    /**
     * Gets the value of the basisCodes property.
     * 
     * @return
     *     possible object is
     *     {@link BasisCodesType }
     *     
     */
    public BasisCodesType getBasisCodes() {
        return basisCodes;
    }

    /**
     * Sets the value of the basisCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link BasisCodesType }
     *     
     */
    public void setBasisCodes(BasisCodesType value) {
        this.basisCodes = value;
    }

    /**
     * Gets the value of the passengerFare property.
     * 
     * @return
     *     possible object is
     *     {@link PassengerFareType }
     *     
     */
    public PassengerFareType getPassengerFare() {
        return passengerFare;
    }

    /**
     * Sets the value of the passengerFare property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengerFareType }
     *     
     */
    public void setPassengerFare(PassengerFareType value) {
        this.passengerFare = value;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionType }
     *     
     */
    public ExtensionType getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionType }
     *     
     */
    public void setExtension(ExtensionType value) {
        this.extension = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

}
