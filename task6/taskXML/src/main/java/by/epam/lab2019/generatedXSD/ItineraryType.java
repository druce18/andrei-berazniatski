
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ItineraryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineraryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Air" type="{}AirType"/>
 *         &lt;element name="PricingInfo" type="{}PricingInfoType"/>
 *         &lt;element name="TicketingInfo" type="{}TicketingInfoType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ItineraryID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryType", propOrder = {
    "air",
    "pricingInfo",
    "ticketingInfo"
})
public class ItineraryType {

    @XmlElement(name = "Air", required = true)
    protected AirType air;
    @XmlElement(name = "PricingInfo", required = true)
    protected PricingInfoType pricingInfo;
    @XmlElement(name = "TicketingInfo", required = true)
    protected TicketingInfoType ticketingInfo;
    @XmlAttribute(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlAttribute(name = "RPH")
    protected String rph;
    @XmlAttribute(name = "ItineraryID")
    protected String itineraryID;

    public ItineraryType() {
        this.air = new AirType();
        this.pricingInfo = new PricingInfoType();
        this.ticketingInfo = new TicketingInfoType();
        this.sequenceNumber = "1";
        this.rph = "1";
        this.itineraryID = "18";
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Itinerary Type :");
        sb.append(air);
        sb.append("pricingInfo=").append(pricingInfo);
        sb.append(ticketingInfo);
        sb.append("sequenceNumber='").append(sequenceNumber).append('\'');
        sb.append(", rph='").append(rph).append('\'');
        sb.append(", itineraryID='").append(itineraryID).append('\'');
        return sb.toString();
    }

    /**
     * Gets the value of the air property.
     * 
     * @return
     *     possible object is
     *     {@link AirType }
     *     
     */
    public AirType getAir() {
        return air;
    }

    /**
     * Sets the value of the air property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirType }
     *     
     */
    public void setAir(AirType value) {
        this.air = value;
    }

    /**
     * Gets the value of the pricingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PricingInfoType }
     *     
     */
    public PricingInfoType getPricingInfo() {
        return pricingInfo;
    }

    /**
     * Sets the value of the pricingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingInfoType }
     *     
     */
    public void setPricingInfo(PricingInfoType value) {
        this.pricingInfo = value;
    }

    /**
     * Gets the value of the ticketingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TicketingInfoType }
     *     
     */
    public TicketingInfoType getTicketingInfo() {
        return ticketingInfo;
    }

    /**
     * Sets the value of the ticketingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketingInfoType }
     *     
     */
    public void setTicketingInfo(TicketingInfoType value) {
        this.ticketingInfo = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the rph property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRPH() {
        return rph;
    }

    /**
     * Sets the value of the rph property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRPH(String value) {
        this.rph = value;
    }

    /**
     * Gets the value of the itineraryID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItineraryID() {
        return itineraryID;
    }

    /**
     * Sets the value of the itineraryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItineraryID(String value) {
        this.itineraryID = value;
    }

}
