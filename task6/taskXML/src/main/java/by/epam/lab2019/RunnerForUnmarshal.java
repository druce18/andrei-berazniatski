package by.epam.lab2019;

import by.epam.lab2019.generatedXSD.ItineraryType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class RunnerForUnmarshal {
    public static void main(String[] args) throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(ItineraryType.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        JAXBElement<ItineraryType> order=(JAXBElement<ItineraryType>)unmarshaller.unmarshal(new File("taskXML\\itinerary.xml"));
        ItineraryType item = order.getValue();

        System.out.println(item);

    }
}
