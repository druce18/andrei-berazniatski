
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PassengerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PassengerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoteSystemPTCs" type="{}RemoteSystemPTCsType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PassengerCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PassengerReference" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="PassengerAge" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PassengerType", propOrder = {
    "remoteSystemPTCs"
})
public class PassengerType {

    @XmlElement(name = "RemoteSystemPTCs", required = true)
    protected RemoteSystemPTCsType remoteSystemPTCs;
    @XmlAttribute(name = "PassengerCode")
    protected String passengerCode;
    @XmlAttribute(name = "PassengerReference")
    protected String passengerReference;
    @XmlAttribute(name = "PassengerAge")
    protected String passengerAge;

    public PassengerType() {
        this.remoteSystemPTCs = new RemoteSystemPTCsType();
        this.passengerCode = "ADT";
        this.passengerReference = "0";
        this.passengerAge = "35";
    }

    /**
     * Gets the value of the remoteSystemPTCs property.
     * 
     * @return
     *     possible object is
     *     {@link RemoteSystemPTCsType }
     *     
     */
    public RemoteSystemPTCsType getRemoteSystemPTCs() {
        return remoteSystemPTCs;
    }

    /**
     * Sets the value of the remoteSystemPTCs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoteSystemPTCsType }
     *     
     */
    public void setRemoteSystemPTCs(RemoteSystemPTCsType value) {
        this.remoteSystemPTCs = value;
    }

    /**
     * Gets the value of the passengerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerCode() {
        return passengerCode;
    }

    /**
     * Sets the value of the passengerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerCode(String value) {
        this.passengerCode = value;
    }

    /**
     * Gets the value of the passengerReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerReference() {
        return passengerReference;
    }

    /**
     * Sets the value of the passengerReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerReference(String value) {
        this.passengerReference = value;
    }

    /**
     * Gets the value of the passengerAge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassengerAge() {
        return passengerAge;
    }

    /**
     * Sets the value of the passengerAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassengerAge(String value) {
        this.passengerAge = value;
    }

}
