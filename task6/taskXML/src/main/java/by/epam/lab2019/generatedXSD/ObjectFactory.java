
package by.epam.lab2019.generatedXSD;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the by.epam.lab2019.generatedXSD package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Itinerary_QNAME = new QName("", "Itinerary");
    private final static QName _TotalTypeExtension_QNAME = new QName("", "Extension");
    private final static QName _TotalTypeTaxes_QNAME = new QName("", "Taxes");
    private final static QName _TotalTypeTotal_QNAME = new QName("", "Total");
    private final static QName _TotalTypeFees_QNAME = new QName("", "Fees");
    private final static QName _TotalTypeBase_QNAME = new QName("", "Base");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: by.epam.lab2019.generatedXSD
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ItineraryType }
     * 
     */
    public ItineraryType createItineraryType() {
        return new ItineraryType();
    }

    /**
     * Create an instance of {@link FeeType }
     * 
     */
    public FeeType createFeeType() {
        return new FeeType();
    }

    /**
     * Create an instance of {@link PassengerQuantityType }
     * 
     */
    public PassengerQuantityType createPassengerQuantityType() {
        return new PassengerQuantityType();
    }

    /**
     * Create an instance of {@link TerminalInformationType }
     * 
     */
    public TerminalInformationType createTerminalInformationType() {
        return new TerminalInformationType();
    }

    /**
     * Create an instance of {@link BookingClassAvailabilityType }
     * 
     */
    public BookingClassAvailabilityType createBookingClassAvailabilityType() {
        return new BookingClassAvailabilityType();
    }

    /**
     * Create an instance of {@link OptionsType }
     * 
     */
    public OptionsType createOptionsType() {
        return new OptionsType();
    }

    /**
     * Create an instance of {@link PricingInfoType }
     * 
     */
    public PricingInfoType createPricingInfoType() {
        return new PricingInfoType();
    }

    /**
     * Create an instance of {@link TaxInfosType }
     * 
     */
    public TaxInfosType createTaxInfosType() {
        return new TaxInfosType();
    }

    /**
     * Create an instance of {@link FareInfoType }
     * 
     */
    public FareInfoType createFareInfoType() {
        return new FareInfoType();
    }

    /**
     * Create an instance of {@link AirType }
     * 
     */
    public AirType createAirType() {
        return new AirType();
    }

    /**
     * Create an instance of {@link PTCsType }
     * 
     */
    public PTCsType createPTCsType() {
        return new PTCsType();
    }

    /**
     * Create an instance of {@link FlightType }
     * 
     */
    public FlightType createFlightType() {
        return new FlightType();
    }

    /**
     * Create an instance of {@link FeesType }
     * 
     */
    public FeesType createFeesType() {
        return new FeesType();
    }

    /**
     * Create an instance of {@link TaxesType }
     * 
     */
    public TaxesType createTaxesType() {
        return new TaxesType();
    }

    /**
     * Create an instance of {@link TaxInfoType }
     * 
     */
    public TaxInfoType createTaxInfoType() {
        return new TaxInfoType();
    }

    /**
     * Create an instance of {@link OptionType }
     * 
     */
    public OptionType createOptionType() {
        return new OptionType();
    }

    /**
     * Create an instance of {@link FlightSupplementalInfoType }
     * 
     */
    public FlightSupplementalInfoType createFlightSupplementalInfoType() {
        return new FlightSupplementalInfoType();
    }

    /**
     * Create an instance of {@link InventorySystemType }
     * 
     */
    public InventorySystemType createInventorySystemType() {
        return new InventorySystemType();
    }

    /**
     * Create an instance of {@link FareInfosType }
     * 
     */
    public FareInfosType createFareInfosType() {
        return new FareInfosType();
    }

    /**
     * Create an instance of {@link SegmentType }
     * 
     */
    public SegmentType createSegmentType() {
        return new SegmentType();
    }

    /**
     * Create an instance of {@link TaxType }
     * 
     */
    public TaxType createTaxType() {
        return new TaxType();
    }

    /**
     * Create an instance of {@link PassengerType }
     * 
     */
    public PassengerType createPassengerType() {
        return new PassengerType();
    }

    /**
     * Create an instance of {@link BaseType }
     * 
     */
    public BaseType createBaseType() {
        return new BaseType();
    }

    /**
     * Create an instance of {@link FeeInfoType }
     * 
     */
    public FeeInfoType createFeeInfoType() {
        return new FeeInfoType();
    }

    /**
     * Create an instance of {@link ExtensionType }
     * 
     */
    public ExtensionType createExtensionType() {
        return new ExtensionType();
    }

    /**
     * Create an instance of {@link TotalType }
     * 
     */
    public TotalType createTotalType() {
        return new TotalType();
    }

    /**
     * Create an instance of {@link FeeInfosType }
     * 
     */
    public FeeInfosType createFeeInfosType() {
        return new FeeInfosType();
    }

    /**
     * Create an instance of {@link TicketingInfoType }
     * 
     */
    public TicketingInfoType createTicketingInfoType() {
        return new TicketingInfoType();
    }

    /**
     * Create an instance of {@link AttributesType }
     * 
     */
    public AttributesType createAttributesType() {
        return new AttributesType();
    }

    /**
     * Create an instance of {@link PTCType }
     * 
     */
    public PTCType createPTCType() {
        return new PTCType();
    }

    /**
     * Create an instance of {@link PassengerFareType }
     * 
     */
    public PassengerFareType createPassengerFareType() {
        return new PassengerFareType();
    }

    /**
     * Create an instance of {@link RemoteSystemPTCsType }
     * 
     */
    public RemoteSystemPTCsType createRemoteSystemPTCsType() {
        return new RemoteSystemPTCsType();
    }

    /**
     * Create an instance of {@link ProviderType }
     * 
     */
    public ProviderType createProviderType() {
        return new ProviderType();
    }

    /**
     * Create an instance of {@link BasisCodesType }
     * 
     */
    public BasisCodesType createBasisCodesType() {
        return new BasisCodesType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItineraryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Itinerary")
    public JAXBElement<ItineraryType> createItinerary(ItineraryType value) {
        return new JAXBElement<ItineraryType>(_Itinerary_QNAME, ItineraryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtensionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Extension", scope = TotalType.class)
    public JAXBElement<ExtensionType> createTotalTypeExtension(ExtensionType value) {
        return new JAXBElement<ExtensionType>(_TotalTypeExtension_QNAME, ExtensionType.class, TotalType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Taxes", scope = TotalType.class)
    public JAXBElement<TaxesType> createTotalTypeTaxes(TaxesType value) {
        return new JAXBElement<TaxesType>(_TotalTypeTaxes_QNAME, TaxesType.class, TotalType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TotalType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Total", scope = TotalType.class)
    public JAXBElement<TotalType> createTotalTypeTotal(TotalType value) {
        return new JAXBElement<TotalType>(_TotalTypeTotal_QNAME, TotalType.class, TotalType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Fees", scope = TotalType.class)
    public JAXBElement<FeesType> createTotalTypeFees(FeesType value) {
        return new JAXBElement<FeesType>(_TotalTypeFees_QNAME, FeesType.class, TotalType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Base", scope = TotalType.class)
    public JAXBElement<BaseType> createTotalTypeBase(BaseType value) {
        return new JAXBElement<BaseType>(_TotalTypeBase_QNAME, BaseType.class, TotalType.class, value);
    }

}
