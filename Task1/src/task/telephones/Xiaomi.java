package task.telephones;

import task.interfaces.UnlockFingerprint;
import task.telephones.AbstractPhone.Phone;
import task.telephones.AbstractPhone.TechnicalSpecifications;

/**
 * Class Xiaomi describes model of Xiaomi.
 * - extends – Phone.
 * - implements – UnlockFingerprint.
 *
 * @author – Andrei Berazniatski.
 */
public class Xiaomi extends Phone implements UnlockFingerprint {
    /**
     * This field contains information about the android shell.
     */
    private String androidShell = Constants.ANDROID_SHELL_XIAOMI;

    public Xiaomi() {
    }

    public Xiaomi(String phoneModel, TechnicalSpecifications specificationsPhone) {
        super(phoneModel, specificationsPhone);
    }

    public Xiaomi(String phoneModel, int cpuFrequency, int ram, int flashMemory) {
        super(phoneModel, cpuFrequency, ram, flashMemory);
    }

    /**
     * This method describes logic of counting how much to run the application in RAM, recording and output to the console.
     */
    @Override
    public void launchFromRAM() {
        int initialTimeLaunchForXiaomi = 5000;
        int timeLaunch = initialTimeLaunchForXiaomi / (getSpecificationsPhone().getRam() * getSpecificationsPhone().getCpuFrequency());
        StringBuilder output = new StringBuilder(Constants.RAM_STARTED + millisecondsInSecondsToString(timeLaunch) + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }

    /**
     * This method describes logic of displaying the state of an object.
     *
     * @return – object state.
     */
    @Override
    public String toString() {
        StringBuilder xiaomi = new StringBuilder(super.toString());
        xiaomi.append(androidShell + Constants.DELIMETER);
        xiaomi.append(recordingMethodResults.toString());
        return xiaomi.toString();
    }

    /**
     * This method describes logic of unlocking the phone on the fingerprint.
     */
    @Override
    public void unlockFingerprint() {
        StringBuilder output = new StringBuilder(Constants.UNLOCK_SUCCESSFUL + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }
}
