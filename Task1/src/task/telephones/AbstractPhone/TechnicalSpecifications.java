package task.telephones.AbstractPhone;

import task.telephones.Constants;

/**
 * Class TechnicalSpecifications describes model of phone specifications
 *
 * @author – Andrei Berazniatski.
 */
public class TechnicalSpecifications {
    /**
     * This field contains information about the frequency of the process.
     */
    private int cpuFrequency;

    /**
     * This field contains information about the amount of RAM.
     */
    private int ram;

    /**
     * This field contains information about the amount of memory.
     */
    private int flashMemory;

    public TechnicalSpecifications() {
    }

    public TechnicalSpecifications(int cpuFrequency, int ram, int flashMemory) {
        this.cpuFrequency = cpuFrequency;
        this.ram = ram;
        this.flashMemory = flashMemory;
    }

    /**
     * This method describes logic of displaying the state of an object.
     *
     * @return – object state.
     */
    @Override
    public String toString() {
        StringBuilder specificationsPhone = new StringBuilder(Constants.SPECIFICATIONS);
        specificationsPhone.append("[ ");
        specificationsPhone.append(Constants.CPU + cpuFrequency + Constants.GHz + Constants.DELIMETER);
        specificationsPhone.append(Constants.RAM + ram + Constants.GB + Constants.DELIMETER);
        specificationsPhone.append(Constants.MEMORY + flashMemory + Constants.GB);
        specificationsPhone.append(" ]");
        return specificationsPhone.toString();
    }

    public int getCpuFrequency() {
        return cpuFrequency;
    }

    public void setCpuFrequency(int cpuFrequency) {
        this.cpuFrequency = cpuFrequency;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getFlashMemory() {
        return flashMemory;
    }

    public void setFlashMemory(int flashMemory) {
        this.flashMemory = flashMemory;
    }
}

