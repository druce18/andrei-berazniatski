package task;

import task.telephones.AbstractPhone.Phone;
import task.telephones.IPhone;
import task.telephones.Pixel;
import task.telephones.Xiaomi;

/**
 * Class Runner describes model of .
 *
 * @author – Andrei Berazniatski.
 */
public class Runner {
    public static void main(String[] args) {
        Xiaomi xiaomi = new Xiaomi("redmi note 7", 2, 4, 64);
        xiaomi.launchFromRAM();
        xiaomi.unlockFingerprint();
        System.out.println(xiaomi.toString()+ "\n");

        IPhone iPhone = new IPhone("X", 3, 2, 64, 12);
        iPhone.launchFromRAM();
        iPhone.unlockPhoneOnFace();
        System.out.println(iPhone.toString() + "\n");

        Pixel pixel = new Pixel("3a", 2, 4, 32);
        pixel.launchFromRAM();
        pixel.unlockFingerprint();
        pixel.unlockPhoneOnFace();
        System.out.println(pixel.toString() + "\n");


    }
}
