package tickets;

public class Constants {
    public final static String PLACE_TRUE = "there is such place";
    public final static String PLACE_FALSE = "there is no such place";
    public final static String BOOKING_SUCCESSFULLY = "booking successfully";
    public final static String PLACE_IS_TAKEN = "place is taken";

}
