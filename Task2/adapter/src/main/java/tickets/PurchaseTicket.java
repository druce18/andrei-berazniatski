package tickets;

public class PurchaseTicket implements BuyTicket {
    private int priceTicket = 50;
    private final static int NUMBER_PLACES = 20;
    private boolean[] places = new boolean[NUMBER_PLACES];

    public int getPrice() {
        return priceTicket;
    }

    public boolean validate(int numberPlace) {
        boolean flag;
        if (numberPlace < NUMBER_PLACES && numberPlace >= 0) {
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }

    public boolean bookTicket(int numberPlace) {
        boolean flag;
        if (!validate(numberPlace)) {
            flag = false;
        } else if (places[numberPlace] == true) {
            flag = false;
        } else {
            flag = true;
            places[numberPlace] = true;
        }
        return flag;
    }

}
