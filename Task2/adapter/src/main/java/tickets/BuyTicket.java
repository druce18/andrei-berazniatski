package tickets;

public interface BuyTicket {

    int getPrice();

    boolean validate(int numberPlace);

    boolean bookTicket(int numberPlace);
}
