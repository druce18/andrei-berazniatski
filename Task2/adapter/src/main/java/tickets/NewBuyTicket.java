package tickets;

public interface NewBuyTicket {

    int calculate(int number);

    void validateTicket(int numberPlace);

    void book(int numberPlace);
}
