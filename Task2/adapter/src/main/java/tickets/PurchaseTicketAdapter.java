package tickets;


public class PurchaseTicketAdapter implements NewBuyTicket {

    private PurchaseTicket purchaseTicket = new PurchaseTicket();

    public int calculate(int number) {
        int priceTickets = number * purchaseTicket.getPrice();
        return priceTickets;
    }

    public void validateTicket(int numberPlace) {
        if (purchaseTicket.validate(numberPlace)) {
            System.out.println(Constants.PLACE_TRUE);
        } else {
            System.out.println(Constants.PLACE_FALSE);
        }
    }

    public void book(int numberPlace) {
        if (purchaseTicket.bookTicket(numberPlace)) {
            System.out.println(Constants.BOOKING_SUCCESSFULLY);
        } else {
            System.out.println(Constants.PLACE_IS_TAKEN);
        }
    }
}
