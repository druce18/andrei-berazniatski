package fruits;

class FruitStore {
    void receptionOfGoods() {
        System.out.println("fruits from stock received");
    }

    void makePayment() {
        System.out.println("payment is made");
    }

    void transferOrderTocCourier() {
        System.out.println("order given to the courier");
    }
}
