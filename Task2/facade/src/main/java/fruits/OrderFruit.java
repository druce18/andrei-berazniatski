package fruits;

public class OrderFruit {
    private FruitWarehouse fruitWarehouse;
    private FruitStore fruitStore;
    private Courier courier;

    public OrderFruit(FruitWarehouse fruitWarehouse, FruitStore fruitStore, Courier courier) {
        this.fruitWarehouse = fruitWarehouse;
        this.fruitStore = fruitStore;
        this.courier = courier;
    }

    public void order() {
        fruitWarehouse.goToWarehouse();
        fruitWarehouse.packFruit();
        fruitWarehouse.sendToStore();
        fruitStore.receptionOfGoods();
        fruitStore.makePayment();
        fruitStore.transferOrderTocCourier();
        courier.deliveryToBuyer();
    }
}
