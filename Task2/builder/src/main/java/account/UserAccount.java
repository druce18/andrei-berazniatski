package account;

public class UserAccount {
    private String login;
    private String password;
    private String eMail;
    private String firstName;
    private String secondName;
    private int age;


    public static class Builder {
        private UserAccount newUserAccount;

        public Builder() {
            newUserAccount = new UserAccount();
        }

        public Builder setLogin(String login) {
            newUserAccount.login = login;
            return this;
        }

        public Builder setPassword(String password) {
            newUserAccount.password = password;
            return this;
        }

        public Builder setEMail(String eMail) {
            newUserAccount.eMail = eMail;
            return this;
        }

        public Builder setFirstName(String firstName) {
            newUserAccount.firstName = firstName;
            return this;
        }

        public Builder setSecondName(String secondName) {
            newUserAccount.secondName = secondName;
            return this;
        }

        public Builder setAge(int age) {
            newUserAccount.age = age;
            return this;
        }

        public UserAccount build() {
            return newUserAccount;
        }

    }
}
