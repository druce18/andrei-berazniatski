import account.UserAccount;

public class Runner {
    public static void main(String[] args) {
        UserAccount userAccountFirst = new UserAccount.Builder()
                .setLogin("druce")
                .setPassword("12345")
                .setEMail("18@gmail.com")
                .build();

        UserAccount userAccountSecond = new UserAccount.Builder()
                .setLogin("druce18")
                .setPassword("123456789")
                .setFirstName("Andrey")
                .setAge(22)
                .build();

        UserAccount userAccountThird = new UserAccount.Builder()
                .setLogin("druce123")
                .setPassword("1234567")
                .build();

    }
}
