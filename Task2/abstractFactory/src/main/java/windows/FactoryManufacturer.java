package windows;

public class FactoryManufacturer {

    public static AbstractFactory getFactory(FactoryType factoryType) {
        if (FactoryType.GOMEL_FACTORY == factoryType) {
            return new GomelFactory();
        } else if (FactoryType.MINSK_FACTORY == factoryType) {
            return new MinskFactory();
        }
        return null;
    }
}
