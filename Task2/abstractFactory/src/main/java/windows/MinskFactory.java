package windows;

public class MinskFactory extends AbstractFactory {

    @Override
    Window getWindow(WindowType name) {
        if (WindowType.PLASTIC == name) {
            return new Plastic();
        } else if (WindowType.SPECIAL == name) {
            return new Special();
        } else if (WindowType.WOODEN == name) {
            return new Wooden();
        }
        return null;
    }
}
