package windows;

public abstract class AbstractFactory {

    abstract Window getWindow(WindowType name);

}
