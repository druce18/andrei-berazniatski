public class BankAccount {
    private int accountNumber;
    private int balance;

    private BankAccountState state;


    private class BankAccountState {
        private int balanceSave;

        public BankAccountState(int balanceSave) {
            this.balanceSave = balanceSave;
        }

        public int getBalanceSave() {
            return balanceSave;
        }
    }


    public BankAccount(int accountNumber, int balance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public void saveBalance() {
        state = new BankAccountState(balance);
    }


    public void rollbackChanges() {
        setBalance(state.getBalanceSave());
    }

    public void transferMoney(int money) {
        int rest = balance - money;
        setBalance(rest);
        if (getBalance() >= 0) {
            System.out.println("you have a positive balance after the transfer");
            System.out.println(toString());
        } else {
            System.out.println("you have a negative balance after the transfer. Transfer canceled");
            String canceledTranslation = toString();
            rollbackChanges();
            System.out.println("state:\n" + toString());
            System.out.println("altered state:\n" + canceledTranslation);
        }
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account number = " + accountNumber + "\nbalance = " + balance + "\n";
    }
}
