package сafe;

public abstract class TeaCoffeeDecorator implements Order {
    protected Order order;

    public TeaCoffeeDecorator(Order order) {
        this.order = order;
    }

    public abstract int getTea();

    public abstract int getCoffee();

}