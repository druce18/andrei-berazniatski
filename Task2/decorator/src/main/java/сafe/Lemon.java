package сafe;

public class Lemon extends TeaCoffeeDecorator {
    private int priceLemon = 2;

    public Lemon(Order order) {
        super(order);
    }

    @Override
    public int getTea() {
        return this.priceLemon + order.getTea();
    }

    @Override
    public int getCoffee() {
        return this.priceLemon + order.getCoffee();
    }
}
