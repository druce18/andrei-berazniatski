package сafe;

public class Milk extends TeaCoffeeDecorator {
    private int priceMilk = 3;

    public Milk(Order order) {
        super(order);
    }

    @Override
    public int getTea() {
        return this.priceMilk + order.getTea();
    }

    @Override
    public int getCoffee() {
        return this.priceMilk+ order.getCoffee();
    }
}
