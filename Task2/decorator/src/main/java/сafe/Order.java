package сafe;

public interface Order {

    int getTea();

    int getCoffee();

}
