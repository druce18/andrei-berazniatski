package сafe;

public class TeaCoffee implements Order {
    private int priceTea;
    private int priceCoffee;

    public TeaCoffee(int priceTea, int priceCoffee) {
        this.priceTea = priceTea;
        this.priceCoffee = priceCoffee;
    }

    public int getTea() {
        return priceTea;
    }

    public int getCoffee() {
        return priceCoffee;
    }
}
