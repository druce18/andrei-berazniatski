package сafe;

public class Sugar extends TeaCoffeeDecorator {
    private int priceSugar = 1;

    public Sugar(Order order) {
        super(order);
    }

    @Override
    public int getTea() {
        return this.priceSugar + order.getTea();
    }

    @Override
    public int getCoffee() {
        return this.priceSugar + order.getCoffee();
    }
}
