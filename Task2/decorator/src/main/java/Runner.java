import сafe.*;

public class Runner {
    public static void main(String[] args) {
        Order teaCoffee = new TeaCoffee(10, 15);
        System.out.println("main price");
        System.out.println(teaCoffee.getTea());
        System.out.println(teaCoffee.getCoffee());

        teaCoffee = new Sugar(teaCoffee);
        System.out.println("price with sugar");
        System.out.println(teaCoffee.getTea());
        System.out.println(teaCoffee.getCoffee());


        teaCoffee = new Milk(teaCoffee);
        System.out.println("price with sugar, milk");
        System.out.println(teaCoffee.getTea());
        System.out.println(teaCoffee.getCoffee());


        teaCoffee = new Lemon(teaCoffee);
        System.out.println("price with sugar, milk, lemon");
        System.out.println(teaCoffee.getTea());
        System.out.println(teaCoffee.getCoffee());
    }
}
