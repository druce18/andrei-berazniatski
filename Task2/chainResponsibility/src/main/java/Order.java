public abstract class Order {

    private Order next;

    public Order setNext(Order next) {
        this.next = next;
        return next;
    }

    public abstract void check();

    public void checkNext() {
        if (true) {
            check();
        }
        if (next != null) {
            next.checkNext();
        }
    }
}
