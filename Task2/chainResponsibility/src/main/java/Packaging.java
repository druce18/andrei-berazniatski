public class Packaging extends Order {
    private int numberPackages;

    public Packaging(int numberPackages) {
        this.numberPackages = numberPackages;
    }

    @Override
    public void check() {
        if (numberPackages > 0) {
            System.out.println("packaging is in stock");
        } else {
            System.out.println("packaging out of stock");
        }
    }
}
