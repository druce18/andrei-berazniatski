public class Product extends Order {
    private int fruitMass;

    public Product(int fruitMass) {
        this.fruitMass = fruitMass;
    }

    @Override
    public void check() {
        if (fruitMass > 0) {
            System.out.println("goods are in stock");
        } else {
            System.out.println("product out of stock");
        }
    }
}
