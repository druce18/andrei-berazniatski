public class Runner {

    public static void main(String[] args) {
        Order order, order1, order2, order3;
        order = new Warehouse();
        order1 = order.setNext(new Product(4));
        order2 = order1.setNext(new Packaging(5));
        order3 = order2.setNext(new Courier());

        order.checkNext();
    }
}
