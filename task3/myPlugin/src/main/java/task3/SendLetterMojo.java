package task3;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Mojo(name = "mail", defaultPhase = LifecyclePhase.INSTALL, threadSafe = true)
public class SendLetterMojo extends AbstractMojo {

    @Parameter(property = "login")
    private String login;

    @Parameter(property = "password")
    private String password;

    @Parameter(property = "toEmail")
    private String toEmail;

    @Parameter(property = "theme")
    private String theme;

    @Parameter(property = "letter")
    private String letter;

    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("sending letter...");

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        Session session = Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(login, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(login));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject(theme);
            message.setText(letter);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

        getLog().info("email sent");

    }
}

