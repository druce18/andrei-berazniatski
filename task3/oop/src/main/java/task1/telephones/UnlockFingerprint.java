package task1.telephones;

/**
 * Interface UnlockFingerprint describes the model of unlocking the phone with a fingerprint.
 *
 * @author – Andrei Berazniatski.
 */
public interface UnlockFingerprint {

    void unlockFingerprint();

}
