package task1.telephones.abstractPhone;

import task1.telephones.Constants;
import task1.telephones.LaunchApplicationFromRAM;


/**
 * Class Phone describes model of phone
 * - implements – LaunchApplicationFromRAM.
 *
 * @author – Andrei Berazniatski.
 */
public abstract class Phone implements LaunchApplicationFromRAM {
    /**
     * This field contains information about the phone model.
     */
    private String phoneModel;

    /**
     * This field contains information about technical specifications phone.
     */
    private TechnicalSpecifications specificationsPhone;

    /**
     * This field contains information about  the methods that implement the actions.
     */
    protected StringBuffer recordingMethodResults = new StringBuffer();


    public Phone() {
    }

    public Phone(String phoneModel, TechnicalSpecifications specificationsPhone) {
        this.phoneModel = phoneModel;
        this.specificationsPhone = specificationsPhone;
    }

    public Phone(String phoneModel, int cpuFrequency, int ram, int flashMemory) {
        this.phoneModel = phoneModel;
        this.specificationsPhone = new TechnicalSpecifications(cpuFrequency, ram, flashMemory);
    }

    /**
     * This method describes logic of displaying the state of an object.
     *
     * @return – object state.
     */
    @Override
    public String toString() {
        StringBuilder phone = new StringBuilder(Constants.PHONE + getClass().getSimpleName() + " " + phoneModel + Constants.DELIMETER);
        phone.append(specificationsPhone.toString());
        return phone.toString();
    }

    /**
     * This method describes logic of counting how much to run the application in RAM.
     */
    @Override
    public void launchFromRAM() {
        System.out.println(Constants.DO_NOT_OVERRIDE);
    }

    /**
     * This method describes logic of translation from milliseconds to seconds.
     *
     * @param milliseconds – name of milliseconds.
     * @return – the resulting line displays seconds.
     */
    public String millisecondsInSecondsToString(int milliseconds) {
        return (milliseconds / 1000) + "." + (milliseconds % 1000) + " seconds";
    }

    /**
     * This method describes logic of recording and output to the console of incoming information.
     *
     * @param output – name of incoming information.
     */
    public void printAndRecordMethodResults(StringBuilder output) {
        recordingMethodResults.append(output.toString());
        System.out.println(output.toString());
    }

    public StringBuffer getRecordingMethodResults() {
        return recordingMethodResults;
    }

    public void setRecordingMethodResults(StringBuffer recordingMethodResults) {
        this.recordingMethodResults = recordingMethodResults;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }


    public TechnicalSpecifications getSpecificationsPhone() {
        return specificationsPhone;
    }

    public void setSpecificationsPhone(TechnicalSpecifications specificationsPhone) {
        this.specificationsPhone = specificationsPhone;
    }

}
