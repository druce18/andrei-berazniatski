package task1.telephones;

import task1.telephones.abstractPhone.Phone;
import task1.telephones.abstractPhone.TechnicalSpecifications;

/**
 * Class Pixel describes model of Pixel.
 * - extends – Phone.
 * - implements – UnlockFaceID, UnlockFingerprint.
 *
 * @author – Andrei Berazniatski.
 */
public class Pixel extends Phone implements UnlockFaceID, UnlockFingerprint {

    public Pixel() {
    }

    public Pixel(String phoneModel, TechnicalSpecifications specificationsPhone) {
        super(phoneModel, specificationsPhone);
    }

    public Pixel(String phoneModel, int cpuFrequency, int ram, int flashMemory) {
        super(phoneModel, cpuFrequency, ram, flashMemory);
    }

    /**
     * This method describes logic of counting how much to run the application in RAM, recording and output to the console.
     */
    @Override
    public void launchFromRAM() {
        int initialTimeLaunchForPixel = 1000;
        StringBuilder output = new StringBuilder(Constants.RAM_STARTED + millisecondsInSecondsToString(initialTimeLaunchForPixel) + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }

    /**
     * This method describes logic of displaying the state of an object.
     *
     * @return – object state.
     */
    @Override
    public String toString() {
        StringBuilder Pixel = new StringBuilder(super.toString());
        Pixel.append(recordingMethodResults.toString());
        return Pixel.toString();
    }

    /**
     * This method describes logic of unlocking the phone in the face.
     */
    @Override
    public void unlockPhoneOnFace() {
        StringBuilder output = new StringBuilder(Constants.UNLOCK_FAILED + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }

    /**
     * This method describes logic of unlocking the phone on the fingerprint.
     */
    @Override
    public void unlockFingerprint() {
        StringBuilder output = new StringBuilder(Constants.UNLOCK_SUCCESSFUL + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }
}
