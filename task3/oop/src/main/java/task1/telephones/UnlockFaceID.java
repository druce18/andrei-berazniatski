package task1.telephones;

/**
 * Interface UnlockFaceID describes the model of unlocking the phone in the face.
 *
 * @author – Andrei Berazniatski.
 */
public interface UnlockFaceID {

    void unlockPhoneOnFace();

}
