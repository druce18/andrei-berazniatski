package task1.telephones;

/**
 * Interface LaunchApplicationFromRAM describes the model of running applications from RAM.
 *
 * @author – Andrei Berazniatski.
 */
public interface LaunchApplicationFromRAM {

    void launchFromRAM();

}
