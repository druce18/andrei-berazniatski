package task1.telephones;

import task1.telephones.abstractPhone.Phone;
import task1.telephones.abstractPhone.TechnicalSpecifications;

/**
 * Class IPhone describes model of iPhone.
 * - extends – Phone.
 * - implements – UnlockFaceID.
 *
 * @author – Andrei Berazniatski.
 */
public class IPhone extends Phone implements UnlockFaceID {
    /**
     * This field contains information about version Apple iOS.
     */
    private int versionIOS;

    public IPhone() {

    }

    public IPhone(String phoneModel, TechnicalSpecifications specificationsPhone, int versionIOS) {
        super(phoneModel, specificationsPhone);
        this.versionIOS = versionIOS;
    }

    public IPhone(String phoneModel, int cpuFrequency, int ram, int flashMemory, int versionIOS) {
        super(phoneModel, cpuFrequency, ram, flashMemory);
        this.versionIOS = versionIOS;
    }

    /**
     * This method describes logic of counting how much to run the application in RAM, recording and output to the console.
     */
    @Override
    public void launchFromRAM() {
        int initialTimeLaunchForIPhone = 1000;
        int timeLaunch = initialTimeLaunchForIPhone / getSpecificationsPhone().getCpuFrequency();
        StringBuilder output = new StringBuilder(Constants.RAM_STARTED + millisecondsInSecondsToString(timeLaunch) + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }

    /**
     * This method describes logic of displaying the state of an object.
     *
     * @return – object state.
     */
    @Override
    public String toString() {
        StringBuilder iPhone = new StringBuilder(super.toString());
        iPhone.append(Constants.APLLE_iOS + versionIOS + Constants.DELIMETER);
        iPhone.append(recordingMethodResults.toString());
        return iPhone.toString();
    }

    /**
     * This method describes logic of unlocking the phone in the face.
     */
    @Override
    public void unlockPhoneOnFace() {
        StringBuilder output = new StringBuilder(Constants.UNLOCK_SUCCESSFUL + Constants.DELIMETER);
        printAndRecordMethodResults(output);
    }

    public int getVersionIOS() {
        return versionIOS;
    }

    public void setVersionIOS(int versionIOS) {
        this.versionIOS = versionIOS;
    }
}
