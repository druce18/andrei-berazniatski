package task1.telephones;

/**
 * Class Constants contains constants.
 *
 * @author – Andrei Berazniatski.
 */
public class Constants {
    public final static String DELIMETER = "; ";
    public final static String ANDROID_OS = " Android";
    public final static String APLLE_iOS = " Apple iOS ";
    public final static String GB = "GB";
    public final static String GHz = "GHz";
    public final static String CPU = "CPU ";
    public final static String RAM = "RAM ";
    public final static String MEMORY = "Memory ";
    public final static String PHONE = "Phone ";
    public final static String ANDROID_SHELL_XIAOMI = " android shell MIUI";
    public final static String SPECIFICATIONS = "Technical Specifications ";
    public final static String RAM_STARTED = "the application from RAM started in ";
    public final static String DO_NOT_OVERRIDE = "this method is not override ";
    public final static String UNLOCK_SUCCESSFUL = "unlock was successful";
    public final static String UNLOCK_FAILED = "unlock failed";

}
