public class CalculationFactorial {
    public static void selectionLoopType(int loopType, int number) {
        if (number < 1) {
            System.out.println("wrong input number");
        } else {
            switch (loopType) {
                case 1:
                    whileLoopType(number);
                    break;
                case 2:
                    doWhileLoopType(number);
                    break;
                case 3:
                    forLoopType(number);
                    break;
                default:
                    System.out.println("wrong input loopType");
                    break;
            }
        }
    }

    private static void whileLoopType(int number) {
        int i = 2;
        int factorial = 1;
        while (i <= number) {
            factorial *= i;
            i++;
        }
        printFactorial(number, factorial);
    }

    private static void doWhileLoopType(int number) {
        int i = 2;
        int factorial = 1;
        do {
            factorial *= i;
            i++;
        } while (i <= number);
        printFactorial(number, factorial);
    }

    private static void forLoopType(int number) {
        int factorial = 1;
        for (int i = 2; i <= number; i++) {
            factorial *= i;
        }
        printFactorial(number, factorial);
    }

    private static void printFactorial(int number, int factorial) {
        System.out.println("Factorial " + number + "! = " + factorial);
    }
}
