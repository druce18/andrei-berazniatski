import java.util.Scanner;

public class Task0 {
    public static void main(String[] args) {
        Scanner in = null;
        int algorithmId = -1;
        int loopType = -1;
        int number = -1;
        try {
            in = new Scanner(System.in);
            System.out.println("Add numbers: algorithmId, loopType, number");
            algorithmId = Integer.parseInt(in.next());
            loopType = Integer.parseInt(in.next());
            number = Integer.parseInt(in.next());
        } catch (NumberFormatException  e) {
            System.err.println("wrong input");
        } finally {
            if (in != null) {
                in.close();
            }
        }

        if (algorithmId == 1) {
            CalculatingFibonacciNumbers.selectionLoopType(loopType, number);
        } else if (algorithmId == 2) {
            CalculationFactorial.selectionLoopType(loopType,number);
        } else {
            System.out.println("wrong input algorithmId");
        }
    }

}
