public class CalculatingFibonacciNumbers {
    public static void selectionLoopType(int loopType, int number) {
        if (number < 1) {
            System.out.println("wrong input number");
        } else {
            switch (loopType) {
                case 1:
                    whileLoopType(number);
                    break;
                case 2:
                    doWhileLoopType(number);
                    break;
                case 3:
                    forLoopType(number);
                    break;
                default:
                    System.out.println("wrong input loopType ");
                    break;
            }
        }
    }

    private static void whileLoopType(int number) {
        int i = 3;
        int n0 = 1;
        int n1 = 1;
        int n2;
        printFirstSecondNumber(number);
        while (i <= number) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            n0 = n1;
            n1 = n2;
            i++;
        }
    }

    private static void doWhileLoopType(int number) {
        int i = 3;
        int n1 = 1;
        int n2 = 1;
        int n3;
        printFirstSecondNumber(number);
        if (number >= i) {
            do {
                n3 = n1 + n2;
                System.out.print(n3 + " ");
                n1 = n2;
                n2 = n3;
                i++;
            } while (i <= number);
        }
    }

    private static void forLoopType(int number) {
        int n0 = 1;
        int n1 = 1;
        int n2;
        printFirstSecondNumber(number);
        for (int i = 3; i <= number; i++) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            n0 = n1;
            n1 = n2;
        }
    }

    private static void printFirstSecondNumber(int number) {
        System.out.println("Fibonacci Numbers:");
        if (number == 1) {
            System.out.print("1 ");
        } else {
            System.out.print("1 1 ");
        }
    }
}