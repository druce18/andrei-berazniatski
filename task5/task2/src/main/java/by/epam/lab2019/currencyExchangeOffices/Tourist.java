package by.epam.lab2019.currencyExchangeOffices;

import java.util.Random;

public class Tourist {
    private int dollars;
    private int localCurrency;
    private int number;


    public Tourist(int dollars, int number) {
        this.dollars = dollars;
        this.localCurrency = 0;
        this.number = number;
    }

    public Tourist() {
        Random random = new Random();
        int money = 100 + random.nextInt(900);
        this.dollars = money;
        this.localCurrency = 0;
    }

    public int getDollars() {
        return dollars;
    }

    public void setDollars(int dollars) {
        this.dollars = dollars;
    }

    public int getLocalCurrency() {
        return localCurrency;
    }

    public void setLocalCurrency(int localCurrency) {
        this.localCurrency = localCurrency;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Tourist #" + number + " have ");
        sb.append(dollars).append(" $ and ");
        sb.append(localCurrency).append(" local money ");
        return sb.toString();
    }
}
