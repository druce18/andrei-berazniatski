package by.epam.lab2019.currencyExchangeOffices;

import java.util.Random;

public class TouristGenerator implements Runnable {
    private Exchange exchangeOffice;
    private Exchange speculator;
    private int touristCount;

    public TouristGenerator(Exchange exchangeOffice, Exchange speculator, int touristCount) {
        this.exchangeOffice = exchangeOffice;
        this.speculator = speculator;
        this.touristCount = touristCount;
    }


    @Override
    public void run() {
        for (int count = 1; count <= touristCount; count++) {
            try {
                if (count % 2 == 0) {
                    exchangeOffice.add(new Tourist(getRandom(100, 900), count));
                } else {
                    speculator.add(new Tourist(getRandom(100, 900), count));
                }
                int time = getRandom(100, 1000);
                Thread.sleep(time);
            } catch (InterruptedException e) {
            }
        }
        exchangeOffice.changeFlagEnd(true);
        speculator.changeFlagEnd(true);
    }

    private int getRandom(int first, int second) {
        Random random = new Random();
        int number = first + random.nextInt(second);
        return number;
    }

}
