package by.epam.lab2019.currencyExchangeOffices;

public class Constants {

    public static final String FOR_TOURIST = "  for tourist #";
    public static final String TOURIST_ADDED_TO_THE_QUEUE_OF_THE_OFFICIAL_EXCHANGER = "Tourist added to the queue of the official exchanger. Queue size is ";
    public static final String READY = "Ready!!! ";
    public static final String EXCHANGE_IS_SUCCESSFUL = "! Exchange is successful!!! ";
    public static final String TOURIST_ADDED_TO_THE_QUEUE_OF_THE_SPECULATOR = "Tourist added to the queue of the speculator. Queue size is ";

}
