package by.epam.lab2019.currencyExchangeOffices;

public class Cashbox implements Runnable {

    private Exchange exchange;

    public Cashbox(Exchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public void run() {
        while (true) {
            try {
                boolean endFlag = exchange.changeAndDelete();
                if (!endFlag) {
                    break;
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
