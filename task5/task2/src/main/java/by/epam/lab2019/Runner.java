package by.epam.lab2019;

import by.epam.lab2019.currencyExchangeOffices.*;
import by.epam.lab2019.currencyExchangeOffices.rate.ChangeRate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runner {
    public static void main(String[] args) {

        Exchange exchangeOffice = new ExchangeOffice();
        Exchange speculator = new Speculator();

        ChangeRate changeRate = new ChangeRate(exchangeOffice, speculator);
        Thread myRate = new Thread(changeRate);
        myRate.setDaemon(true);

        myRate.start();

        TouristGenerator touristGenerator = new TouristGenerator(exchangeOffice, speculator, 30);

        Cashbox cashbox1 = new Cashbox(exchangeOffice);
        Cashbox cashbox2 = new Cashbox(speculator);

        ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        service.execute(touristGenerator);
        service.execute(cashbox1);
        service.execute(cashbox2);

        service.shutdown();

    }
}
