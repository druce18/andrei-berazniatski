package by.epam.lab2019.currencyExchangeOffices;

import by.epam.lab2019.currencyExchangeOffices.rate.Rate;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class ExchangeOffice implements Exchange {
    private static final Logger logger = Logger.getLogger(ExchangeOffice.class.getName());
    private Rate rate;
    private BlockingQueue<Tourist> touristQueue;
    private Lock lock;
    boolean flagEnd = false;

    public ExchangeOffice() {
        touristQueue = new ArrayBlockingQueue<>(3, true);
        rate = new Rate();
        this.lock = new ReentrantLock();
    }

    @Override
    public boolean add(Tourist element) {
        try {
            touristQueue.put(element);
            logger.info(Constants.TOURIST_ADDED_TO_THE_QUEUE_OF_THE_OFFICIAL_EXCHANGER + touristQueue.size());
        } catch (InterruptedException e) {
        }
        return true;
    }

    @Override
    public boolean changeAndDelete() {
        if (flagEnd && touristQueue.isEmpty()) {
            return false;
        }

        lock.lock();
        try {
            Tourist tourist = touristQueue.take();
            Rate rateTourist = new Rate(rate.getValue());
            logger.info(rateTourist.toString() + Constants.FOR_TOURIST + tourist.getNumber());
            logger.info(Constants.READY + tourist.toString());
            rateTourist.exchangeLocalCurrency(tourist);
            Thread.sleep(3000);
            logger.info(Constants.EXCHANGE_IS_SUCCESSFUL + tourist);
        } catch (InterruptedException e) {
        } finally {
            lock.unlock();
        }

        return true;
    }

    @Override
    public void changeRate(Rate rate) {
        this.rate = rate;
    }

    @Override
    public int getRate() {
        return rate.getValue();
    }

    @Override
    public boolean getFlagEnd() {
        return flagEnd;
    }

    @Override
    public void changeFlagEnd(boolean flag) {
        this.flagEnd = flag;
    }
}
