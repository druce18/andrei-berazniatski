package by.epam.lab2019.currencyExchangeOffices;

import by.epam.lab2019.currencyExchangeOffices.rate.Rate;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class Speculator implements Exchange {
    private static final Logger logger = Logger.getLogger(Speculator.class.getName());
    private Rate rate;
    private Queue<Tourist> touristQueue;
    private Lock lock;
    boolean flagEnd = false;

    public Speculator() {
        this.touristQueue = new ConcurrentLinkedQueue();
        this.rate = new Rate();
        this.lock = new ReentrantLock();
    }

    @Override
    public boolean add(Tourist element) {
        touristQueue.add(element);
        logger.info(Constants.TOURIST_ADDED_TO_THE_QUEUE_OF_THE_SPECULATOR + touristQueue.size());
        return true;
    }

    @Override
    public boolean changeAndDelete() {
        if (flagEnd && touristQueue.isEmpty()) {
            return false;
        }

        lock.lock();
        try {
            Tourist tourist = touristQueue.poll();
            if (tourist != null) {
                Rate rateTourist = rate;
                logger.info(rateTourist.toString() + Constants.FOR_TOURIST + tourist.getNumber());
                logger.info(Constants.READY + tourist.toString());
                rateTourist.exchangeLocalCurrency(tourist);
                Thread.sleep(2000);
                logger.info(Constants.EXCHANGE_IS_SUCCESSFUL + tourist);
            }
        } catch (InterruptedException e) {
        } finally {
            lock.unlock();
        }

        return true;
    }

    @Override
    public void changeRate(Rate rate) {
        this.rate = rate;
    }

    @Override
    public int getRate() {
        return rate.getValue();
    }

    @Override
    public boolean getFlagEnd() {
        return flagEnd;
    }

    @Override
    public void changeFlagEnd(boolean flag) {
        this.flagEnd = flag;
    }
}