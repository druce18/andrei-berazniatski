package by.epam.lab2019.currencyExchangeOffices.rate;

import by.epam.lab2019.currencyExchangeOffices.Tourist;

public class Rate {
    private int value;

    public Rate(int value) {
        this.value = value;
    }

    public Rate() {
        this.value = 100;
    }

    public int getValue() {
        return value;
    }

    public void exchangeLocalCurrency(Tourist tourist) {
        int localMoney = value * tourist.getDollars();
        tourist.setLocalCurrency(localMoney);
        tourist.setDollars(0);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("rate is 1$ to ");
        sb.append(value);
        return sb.toString();
    }
}
