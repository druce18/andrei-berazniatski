package by.epam.lab2019;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Logger;

public class ForkJoinRecursiveSum {
    private static final int N_THREADS = 8;
    private static final int BILLION = 1_000_000_000;
    private static final Logger logger = Logger.getLogger(ForkJoinRecursiveSum.class.getName());

    public static void main(final String[] arguments) {

        long startTime2 = System.currentTimeMillis();
        long sum = 0;
        for (int i = 0; i <= BILLION; i++) {
            sum += i;
        }
        long endTime2 = System.currentTimeMillis();
        logger.info("Time ->  " + (endTime2 - startTime2));
        logger.info("Result = " + sum);


        long startTime1 = System.currentTimeMillis();
        ForkJoinPool forkJoinPool = new ForkJoinPool(N_THREADS);
        Long result = forkJoinPool.invoke(new Sum(0, BILLION));
        long endTime1 = System.currentTimeMillis();
        logger.info("Time Fork/Join ->  " + (endTime1 - startTime1));
        logger.info("Result = " + result);

    }

    static class Sum extends RecursiveTask<Long> {

        long start;
        long end;

        Sum(long start, long end) {
            this.start = start;
            this.end = end;
        }

        protected Long compute() {
            if (end - start <= BILLION / N_THREADS) {
                long localSum = 0;
                for (long i = start; i <= end; i++) {
                    localSum += i;
                }
                return localSum;
            } else {
                long mid = start + (end - start) / 2;
                Sum left = new Sum(start, mid);
                left.fork();
                Sum right = new Sum(mid + 1, end);
                long resultSecond = right.compute();
                long resultFirst = left.join();
                return resultFirst + resultSecond;
            }
        }
    }
}
