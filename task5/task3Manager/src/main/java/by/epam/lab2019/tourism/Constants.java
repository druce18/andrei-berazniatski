package by.epam.lab2019.tourism;

public class Constants {

    public static final int PLACES_IN_BUS = 40;
    public static final int NUMBER_PHASES = 6;
    public final static String BOARDING_TOURISTS_ON_BUS = " Picking up and boarding tourists on a bus. ";
    public final static String TOURISTS_BOARDED_BUS = " Tourists boarded the bus. ";
    public final static String PRAGUE = " The first stop is Prague. Tourists rush to the Old Town to drink a pint of dark Czech beer. ";
    public final static String PARIS = " Tourists arrive in Paris, the Louvre. They rush to look at the Mona Lisa smile. ";
    public final static String ROME = " Eternal Rome stop where tourists try pizza or pasta. ";
    public final static String SPAIN = " The last stop is the coast of Spain, where everyone can relax on the beach at the same time.";
    public final static String PHOTO = " Total number of photos taken ";
    public final static String END_TRAVEL = " End of journey. ";
    public final static String BUS_GOES_TO_THE_NEXT_CITY = " The bus goes to the next city. ";
    public final static String GROUP = "\nGroup #";



}
