package by.epam.lab2019.tourism;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TravelPoints {
    private static final boolean[] PARKING_PLACES = new boolean[5];
    private static final Semaphore SEMAPHORE = new Semaphore(5, true);
    private static final CyclicBarrier PIZZA = new CyclicBarrier(5, new Maintenance(3000));
    private static final CyclicBarrier PASTA = new CyclicBarrier(5, new Maintenance(5000));
    private static Lock lock = new ReentrantLock();

    public static void drinkBeerPrague(Tourist tourist) {
        lock.lock();
        try {
            Thread.sleep(tourist.getTimeToEatOrRelax());
            tourist.incCounterPhoto();
        } catch (InterruptedException e) {
        } finally {
            lock.unlock();
        }
    }

    public static void watchParis(Tourist tourist) {
        try {
            SEMAPHORE.acquire();

            int parkingNumber = -1;
            synchronized (PARKING_PLACES) {
                for (int i = 0; i < 5; i++)
                    if (!PARKING_PLACES[i]) {
                        PARKING_PLACES[i] = true;
                        parkingNumber = i;
                        break;
                    }
            }

            Thread.sleep(1000);

            synchronized (PARKING_PLACES) {
                PARKING_PLACES[parkingNumber] = false;
            }

            SEMAPHORE.release();
            tourist.incCounterPhoto();
        } catch (InterruptedException e) {
        }

    }

    public static void eatPizzaOrPastaRome(Tourist tourist) {
        try {
            if (tourist.getTouristNumber() % 2 == 0) {
                PASTA.await();
                Thread.sleep(tourist.getTimeToEatOrRelax());
            } else {
                PIZZA.await();
                Thread.sleep(tourist.getTimeToEatOrRelax());
            }
            tourist.incCounterPhoto();
        } catch (Exception e) {
        }
    }

    public static void relaxSpain(Tourist tourist) {
        try {
            Thread.sleep(tourist.getTimeToEatOrRelax());
            tourist.incCounterPhoto();
        } catch (InterruptedException e) {
        }
    }

    public static class Maintenance implements Runnable {
        private int serviceTime;

        public Maintenance(int serviceTime) {
            this.serviceTime = serviceTime;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(serviceTime);
            } catch (InterruptedException e) {
            }
        }
    }

}
