package by.epam.lab2019.tourism;

import java.util.Random;

import static by.epam.lab2019.tourism.Manager.PHASER;


public class Tourist extends Thread {
    private boolean isInTravel;
    private int counterPhoto;
    private int timeToEatOrRelax;
    private int touristNumber;

    public Tourist(int touristNumber) {
        this.isInTravel = false;
        this.counterPhoto = 0;
        this.timeToEatOrRelax = getRandom(1000, 4000);
        this.touristNumber = touristNumber;
    }

    @Override
    public void run() {
        try {
            while (PHASER.getPhase() < Constants.NUMBER_PHASES) {
                int phase = PHASER.getPhase();
                if (phase == 0) {
                    this.setInTravel(true);
                    PHASER.arriveAndAwaitAdvance();
                } else if (phase == 1) {
                    TravelPoints.drinkBeerPrague(this);
                    PHASER.arriveAndAwaitAdvance();
                } else if (phase == 2) {
                    TravelPoints.watchParis(this);
                    PHASER.arriveAndAwaitAdvance();
                } else if (phase == 3) {
                    TravelPoints.eatPizzaOrPastaRome(this);
                    PHASER.arriveAndAwaitAdvance();
                } else if (phase == 4) {
                    TravelPoints.relaxSpain(this);
                    Manager.addPhoto(this);
                    PHASER.arriveAndAwaitAdvance();
                } else if (phase == 5) {
                    this.setInTravel(false);
                    break;
                }
            }
            PHASER.arriveAndDeregister();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }


    private int getRandom(int first, int second) {
        Random random = new Random();
        int number = first + random.nextInt(second);
        return number;
    }

    public void incCounterPhoto() {
        this.counterPhoto++;
    }

    public boolean isInTravel() {
        return isInTravel;
    }

    public void setInTravel(boolean inTravel) {
        isInTravel = inTravel;
    }

    public int getCounterPhoto() {
        return counterPhoto;
    }

    public void setCounterPhoto(int counterPhoto) {
        this.counterPhoto = counterPhoto;
    }

    public int getTimeToEatOrRelax() {
        return timeToEatOrRelax;
    }


    public int getTouristNumber() {
        return touristNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Tourist  ");
        sb.append("time = ").append(timeToEatOrRelax);
        sb.append(";  photo= ").append(counterPhoto);
        sb.append("; # ").append(touristNumber);
        return sb.toString();
    }
}
