package by.epam.lab2019.tourism;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class Manager {
    private static final Logger logger = Logger.getLogger(Manager.class.getName());
    private static int totalCounterPhoto;
    private static Lock lock = new ReentrantLock();
    public static Phaser PHASER = new Phaser(1);
    private int numberGroups;


    public Manager(int numberGroups) {
        this.numberGroups = numberGroups;
        this.totalCounterPhoto = 0;
    }

    public Manager() {
        this(1);
    }


    public void wayBus() {
        for (int count = 1; count <= numberGroups; count++) {
            PHASER = new Phaser(1);
            logger.info(Constants.GROUP + count);
            totalCounterPhoto = 0;
            List<Tourist> touristList = new ArrayList<>();
            for (int i = 0; i < Constants.PLACES_IN_BUS; i++) {
                touristList.add(new Tourist(i));
            }

            for (int i = 0; i < Constants.NUMBER_PHASES; i++) {
                switch (i) {
                    case 0:
                        logger.info(Constants.BOARDING_TOURISTS_ON_BUS);
                        for (Tourist tourist : touristList) {
                            PHASER.register();
                            tourist.start();
                        }
                        PHASER.arriveAndAwaitAdvance();
                        logger.info(Constants.TOURISTS_BOARDED_BUS + Constants.BUS_GOES_TO_THE_NEXT_CITY);
                        break;
                    case 1:
                        logger.info(Constants.PRAGUE);
                        PHASER.arriveAndAwaitAdvance();
                        logger.info(Constants.TOURISTS_BOARDED_BUS + Constants.BUS_GOES_TO_THE_NEXT_CITY);
                        break;
                    case 2:
                        logger.info(Constants.PARIS);
                        PHASER.arriveAndAwaitAdvance();
                        logger.info(Constants.TOURISTS_BOARDED_BUS + Constants.BUS_GOES_TO_THE_NEXT_CITY);
                        break;
                    case 3:
                        logger.info(Constants.ROME);
                        PHASER.arriveAndAwaitAdvance();
                        logger.info(Constants.TOURISTS_BOARDED_BUS + Constants.BUS_GOES_TO_THE_NEXT_CITY);
                        break;
                    case 4:
                        logger.info(Constants.SPAIN);
                        PHASER.arriveAndAwaitAdvance();
                        logger.info(Constants.PHOTO + totalCounterPhoto);
                        logger.info(Constants.TOURISTS_BOARDED_BUS + Constants.BUS_GOES_TO_THE_NEXT_CITY);
                        break;
                    case 5:
                        logger.info(Constants.END_TRAVEL);
                        PHASER.arriveAndDeregister();
                        break;
                }
            }
        }
    }

    public static void addPhoto(Tourist tourist) {
        lock.lock();
        try {
            Manager.totalCounterPhoto += tourist.getCounterPhoto();
            tourist.setCounterPhoto(0);
            Thread.sleep(100);
        } catch (InterruptedException e) {
        } finally {
            lock.unlock();
        }

    }
}
