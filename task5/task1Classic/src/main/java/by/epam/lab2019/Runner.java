package by.epam.lab2019;

import by.epam.lab2019.currencyExchangeOffices.*;
import by.epam.lab2019.currencyExchangeOffices.rate.ChangeRate;

public class Runner {
    public static void main(String[] args) {

        Exchange exchangeOffice = new ExchangeOffice();
        Exchange speculator = new Speculator();

        ChangeRate changeRate = new ChangeRate(exchangeOffice, speculator);
        Thread myRate = new Thread(changeRate);
        myRate.setDaemon(true);

        myRate.start();

        TouristGenerator touristGenerator = new TouristGenerator(exchangeOffice, speculator, 30);
        Thread myTouristGenerator = new Thread(touristGenerator);

        Cashbox cashbox1 = new Cashbox(exchangeOffice);
        Cashbox cashbox2 = new Cashbox(speculator);
        Thread myCashbox1 = new Thread(cashbox1);
        Thread myCashbox2 = new Thread(cashbox2);

        myTouristGenerator.start();

        myCashbox1.start();
        myCashbox2.start();

    }
}
