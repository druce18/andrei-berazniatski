package by.epam.lab2019.currencyExchangeOffices;

public class Cashbox implements Runnable {

    private Exchange exchange;

    public Cashbox(Exchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                boolean flagEmpty = exchange.changeAndDelete();
                if (!flagEmpty && exchange.getFlagEnd()) {
                    break;
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
