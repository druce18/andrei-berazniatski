package by.epam.lab2019.currencyExchangeOffices.rate;

import by.epam.lab2019.currencyExchangeOffices.Exchange;
import by.epam.lab2019.currencyExchangeOffices.ExchangeOffice;
import by.epam.lab2019.currencyExchangeOffices.Speculator;

import java.util.Random;

public class ChangeRate implements Runnable {
    private Exchange exchangeOffice;
    private Exchange speculator;

    public ChangeRate(Exchange exchangeOffice, Exchange speculator) {
        this.exchangeOffice = exchangeOffice;
        this.speculator = speculator;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Random random = new Random();
                int value = exchangeOffice.getRate() + random.nextInt(7) - random.nextInt(7);
                int valueSpeculator = value * (100 - 5) / 100;
                exchangeOffice.changeRate(new Rate(value));
                speculator.changeRate(new Rate(valueSpeculator));
                int time = 5000 + random.nextInt(5000);
                Thread.sleep(time);
            } catch (InterruptedException e) {
            }
        }
    }

}
