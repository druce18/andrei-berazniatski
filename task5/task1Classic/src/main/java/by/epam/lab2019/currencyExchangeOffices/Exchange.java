package by.epam.lab2019.currencyExchangeOffices;

import by.epam.lab2019.currencyExchangeOffices.rate.Rate;

public interface Exchange {

    boolean add(Tourist tourist);

    boolean changeAndDelete();

    void changeRate(Rate rate);

    int getRate();

    boolean getFlagEnd();

    void changeFlagEnd(boolean flag);

}
