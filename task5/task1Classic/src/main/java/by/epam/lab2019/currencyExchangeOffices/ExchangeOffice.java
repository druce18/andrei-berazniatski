package by.epam.lab2019.currencyExchangeOffices;

import by.epam.lab2019.currencyExchangeOffices.rate.Rate;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.logging.Logger;

public class ExchangeOffice implements Exchange {
    private static final Logger logger = Logger.getLogger(ExchangeOffice.class.getName());

    private Rate rate;
    private Queue<Tourist> touristQueue;
    private static final int MAX_TOURIST = 3;
    private static final int MIN_TOURIST = 0;
    private int touristsCounter = 0;
    boolean flagEnd = false;

    public ExchangeOffice() {
        touristQueue = new ArrayDeque<>();
        rate = new Rate();
    }

    @Override
    public synchronized boolean add(Tourist element) {
        try {
            if (touristsCounter < MAX_TOURIST) {
                touristQueue.add(element);
                logger.info(Constants.TOURIST_ADDED_TO_THE_QUEUE_OF_THE_OFFICIAL_EXCHANGER + touristQueue.size());
                touristsCounter++;
                notifyAll();
            } else {
                logger.info(Constants.THERE_IS_NO_PLACE_IN_THE_QUEUE_OF_THE_OFFICIAL_EXCHANGER + touristQueue.size());
                wait();
                return false;
            }
        } catch (InterruptedException e) {
        }
        return true;
    }

    @Override
    public synchronized boolean changeAndDelete() {
        try {
            if (touristsCounter > MIN_TOURIST) {
                touristsCounter--;
                Tourist tourist = touristQueue.remove();
                Rate rateTourist = new Rate(rate.getValue());
                logger.info(rateTourist.toString() + Constants.FOR_TOURIST + tourist.getNumber());
                logger.info(Constants.READY + tourist.toString());
                rateTourist.exchangeLocalCurrency(tourist);
                Thread.sleep(3000);
                logger.info(Constants.EXCHANGE_IS_SUCCESSFUL + tourist);
                notifyAll();
                return true;
            } else if (!flagEnd) {
                logger.info(Constants.QUEUE_IS_EMPTY);
                wait();
                return true;
            }
        } catch (InterruptedException e) {
        }
        return false;
    }

    @Override
    public void changeRate(Rate rate) {
        this.rate = rate;
    }

    @Override
    public int getRate() {
        return rate.getValue();
    }

    @Override
    public boolean getFlagEnd() {
        return flagEnd;
    }

    @Override
    public void changeFlagEnd(boolean flag) {
        this.flagEnd = flag;
    }
}
