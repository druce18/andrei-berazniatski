package by.epam.lab2019.currencyExchangeOffices;

import by.epam.lab2019.currencyExchangeOffices.rate.Rate;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.logging.Logger;

public class Speculator implements Exchange {
    private static final Logger logger = Logger.getLogger(Speculator.class.getName());
    private Rate rate;
    private Queue<Tourist> touristQueue;
    boolean flagEnd = false;

    public Speculator() {
        this.touristQueue = new ArrayDeque<>();
        this.rate = new Rate();
    }

    @Override
    public synchronized boolean add(Tourist element) {
        touristQueue.add(element);
        logger.info(Constants.TOURIST_ADDED_TO_THE_QUEUE_OF_THE_SPECULATOR + touristQueue.size());
        return true;
    }

    @Override
    public synchronized boolean changeAndDelete() {
        try {
            if (!touristQueue.isEmpty()) {
                Tourist tourist = touristQueue.remove();
                Rate rateTourist = rate;
                logger.info(rateTourist.toString() + Constants.FOR_TOURIST + tourist.getNumber());
                logger.info(Constants.READY + tourist.toString());
                rateTourist.exchangeLocalCurrency(tourist);
                Thread.sleep(2000);
                logger.info(Constants.EXCHANGE_IS_SUCCESSFUL + tourist);
                return true;
            } else if (!flagEnd) {
                logger.info(Constants.QUEUE_IS_EMPTY);
            }
        } catch (InterruptedException e) {
        }
        return false;
    }

    @Override
    public void changeRate(Rate rate) {
        this.rate = rate;
    }

    @Override
    public int getRate() {
        return rate.getValue();
    }

    @Override
    public boolean getFlagEnd() {
        return flagEnd;
    }

    @Override
    public void changeFlagEnd(boolean flag) {
        this.flagEnd = flag;
    }
}