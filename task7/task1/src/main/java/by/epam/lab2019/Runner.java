package by.epam.lab2019;

import by.epam.lab2019.students.Groups;
import by.epam.lab2019.students.Random;
import by.epam.lab2019.students.Student;

import java.util.Comparator;
import java.util.List;
import java.util.function.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
    private static final Logger logger = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {

        logger.info("Point #2");
        List<Student> students = Stream.generate(Student::new)
                .limit(10)
                .collect(Collectors.toList());
        students.forEach(student -> logger.info(student.toString()));


        logger.info("Point #3, 4");
        Comparator<Student> byID = (Student o1, Student o2) -> o1.getId() - o2.getId();
        List<Student> sortedStudents1 = students.stream()
                .sorted(byID)
                .collect(Collectors.toList());
        logger.info("Sorted #1: ");
        sortedStudents1.forEach(student -> logger.info(student.toString()));


        Comparator<Student> byID2 = (Student o1, Student o2) -> {
            if (o1.getCourse() == o2.getCourse()) {
                return o1.getSemester() - o2.getSemester();
            } else {
                return o1.getCourse() - o2.getCourse();
            }
        };
        List<Student> sortedStudents2 = students.stream()
                .sorted(byID2)
                .collect(Collectors.toList());
        logger.info("Sorted #2: ");
        sortedStudents2.forEach(student -> logger.info(student.toString()));


        logger.info("Point #5");
        Predicate<Student> studentCheckLastCourse = student -> {
            if (student.getCourse() == 4 && student.getSemester() == 2) {
                return true;
            }
            return false;
        };

        UnaryOperator<Student> studentAddCourse = student -> {
            student.setCourse(student.getCourse() + 1);
            return student;
        };

        Supplier<Student> studentNew = Student::new;

        Function<Student, Groups> returnGroup = Student::getGroup;

        Consumer<List<Student>> employer = studentsList -> {
            List<Student> studentsGetJob = studentsList.stream()
                    .filter(studentCheckLastCourse)
                    .limit(3)
                    .collect(Collectors.toList());
            logger.info("Students got a job distribution " + studentsGetJob.toString());
        };

        Runnable runnableEndImpl = () -> {
            logger.info("I implemented 6 interfaces");
        };
        runnableEndImpl.run();


        logger.info("Point #6, 7");
        Random<Double> randomDouble = () -> {
            return Math.random();
        };

        Random<Groups> randomGroup = () -> {
            int number = Random.getNumberBetween(0, Groups.values().length);
            return Groups.values()[number];
        };


        Random<Student> randomStudent = () -> {
            int id = Random.getNumberBetween(100000, 900000);
            String name = "Student # " + id;
            boolean gender;
            if (Random.getNumberBetween(0, 2) == 0) {
                gender = false;
            } else {
                gender = true;
            }
            Groups group = randomGroup.getRandom();
            int course = Random.getNumberBetween(1, 4);
            int semester = Random.getNumberBetween(1, 2);
            return new Student(id, name, gender, group, course, semester);
        };

    }
}
