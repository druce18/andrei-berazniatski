package by.epam.lab2019.students;

public class Student implements Random<Groups> {

    private int id;
    private String name;
    private boolean gender;
    private Groups group;
    private int course;
    private int semester;

    public Student() {
        this.id = Random.getNumberBetween(100000, 900000);
        this.name = "Student # " + id;
        if (Random.getNumberBetween(0, 2) == 0) {
            this.gender = false;
        } else {
            this.gender = true;
        }
        this.group = getRandom();
        this.course = Random.getNumberBetween(1, 4);
        this.semester = Random.getNumberBetween(1, 2);
    }

    public Student(int id, String name, boolean gender, Groups group, int course, int semester) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.group = group;
        this.course = course;
        this.semester = semester;
    }

    @Override
    public Groups getRandom() {
        int number = Random.getNumberBetween(0, Groups.values().length);
        return Groups.values()[number];
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(name);
        if (gender) {
            sb.append(";  gender: MAN");
        } else {
            sb.append(";  gender: WOMAN");
        }
        sb.append(";  name group: ").append(group);
        sb.append(";  course # ").append(course);
        sb.append(";  semester # ").append(semester);
        return sb.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public Groups getGroup() {
        return group;
    }

    public void setGroup(Groups group) {
        this.group = group;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }
}
