package by.epam.lab2019;

import by.epam.lab2019.creation.Author;
import by.epam.lab2019.creation.Book;

import java.util.Comparator;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class Runner {
    private static final Logger logger = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {

        logger.info("Point #2");
        Author[] authors = Stream.generate(Author::new)
                .limit(15)
                .peek(Author::generateBooks)
                .toArray(Author[]::new);
        logger.info("Authors: ");
        Stream.of(authors)
                .forEach(author -> logger.info(author.toString()));

        Book[] books = Stream.generate(Book::new)
                .limit(15)
                .peek(Book::generateBooks)
                .toArray(Book[]::new);
        logger.info("Books: ");
        Stream.of(books)
                .forEach(book -> logger.info(book.toString()));


        logger.info("Point #3.1");
        boolean result1 = Stream.of(books)
                .anyMatch(x -> x.getPagesNumber() > 200);
        if (result1) {
            logger.info("There are books with more than 200 pages ");
        } else {
            logger.info("There is no book with more than 200 pages ");
        }


        logger.info("Point #3.2");
        boolean result2 = Stream.of(books)
                .allMatch(x -> x.getPagesNumber() > 200);
        if (result2) {
            logger.info("All books have over 200 pages ");
        } else {
            logger.info("Not all books have more than 200 pages ");
        }


        logger.info("Point #3.3");
        Book minPages = Stream.of(books)
                .min(Comparator.comparingInt(Book::getPagesNumber)).get();
        Stream.of(books)
                .filter(book -> book.getPagesNumber() == minPages.getPagesNumber())
                .forEach(book -> logger.info("Book with fewer pages: " + book.toString()));

        Book maxPages = Stream.of(books)
                .max(Comparator.comparingInt(Book::getPagesNumber)).get();
        Stream.of(books)
                .filter(book -> book.getPagesNumber() == maxPages.getPagesNumber())
                .forEach(book -> logger.info("Books with the most pages: " + book.toString()));


        logger.info("Point #3.4");
        Stream.of(books)
                .filter(book -> book.getAuthors().size() == 1)
                .forEach(book -> logger.info("Book with 1 author " + book.toString()));


        logger.info("Point #3.5");
        logger.info("Sort on pages");
        Stream.of(books)
                .sorted(Comparator.comparingInt(Book::getPagesNumber))
                .forEach(book -> logger.info(book.toString()));

        logger.info("Sort on name book");
        Stream.of(books)
                .sorted(Comparator.comparing(book -> book.getTitle().toString()))
                .forEach(book -> logger.info(book.toString()));


        logger.info("Point #3.6");
        logger.info("list of all unique book titles");
        Stream.of(books)
                .distinct()
                .map(book -> book.getTitle().toString())
                .forEach(logger::info);


        logger.info("Point #3.7");
        logger.info("A unique list of book authors with less than 200 pages.");
        Stream.of(books)
                .filter(book -> book.getPagesNumber() < 200)
                .flatMap(book -> book.getAuthors().stream())
                .distinct()
                .map(author -> author.getName().toString())
                .forEach(logger::info);

    }
}
