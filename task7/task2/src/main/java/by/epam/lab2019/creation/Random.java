package by.epam.lab2019.creation;

@FunctionalInterface
public interface Random<T> {

    T getRandom();

    default int getRandomInt() {
        java.util.Random random = new java.util.Random();
        return random.nextInt();
    }

    static int getNumberBetween(int first, int second) {
        java.util.Random random = new java.util.Random();
        int number = first + random.nextInt(second);
        return number;
    }

}
