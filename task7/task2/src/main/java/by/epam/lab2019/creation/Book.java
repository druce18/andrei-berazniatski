package by.epam.lab2019.creation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Book implements Random<BooksNames> {

    private BooksNames title;
    private int pagesNumber;
    private List<Author> authors;

    public Book() {
        this.title = getRandom();
        this.pagesNumber = Random.getNumberBetween(50, 500);
        this.authors = new ArrayList<>();
    }

    public void generateBooks() {
        this.authors = Stream.generate(Author::new)
                .distinct()
                .limit(Random.getNumberBetween(1, 3))
                .collect(Collectors.toList());
    }

    @Override
    public BooksNames getRandom() {
        int number = Random.getNumberBetween(0, BooksNames.values().length);
        return BooksNames.values()[number];
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Book: ");
        sb.append("title= ").append(title);
        sb.append(", pages=").append(pagesNumber);
        sb.append(", authors=").append(authors);
        return sb.toString();
    }

    public BooksNames getTitle() {
        return title;
    }

    public void setTitle(BooksNames title) {
        this.title = title;
    }

    public int getPagesNumber() {
        return pagesNumber;
    }

    public void setPagesNumber(int pagesNumber) {
        this.pagesNumber = pagesNumber;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return title == book.title;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}
