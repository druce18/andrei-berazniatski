package by.epam.lab2019.creation;

public enum AuthorNames {
    S_King, George_Martin, Leo_Tolstoy, George_Orwell, James_Joyce, Vladimir_Nabokov,
    William_Faulkner, Ralph_Allison, John_Steinbeck, Michael_Bulgakov, Fedor_Dostoevsky,
    Antoine_DeSaint_Exupery, Mikhail_Lermontov


}
