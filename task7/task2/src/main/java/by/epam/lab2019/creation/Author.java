package by.epam.lab2019.creation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Author implements Random<AuthorNames> {

    private AuthorNames name;
    private int age;
    private List<Book> books;

    public Author() {
        this.name = getRandom();
        this.age = Random.getNumberBetween(10, 90);
        this.books = new ArrayList<>();
    }

    public void generateBooks() {
        this.books = Stream.generate(Book::new)
                .distinct()
                .limit(Random.getNumberBetween(1, 3))
                .collect(Collectors.toList());
    }

    @Override
    public AuthorNames getRandom() {
        int number = Random.getNumberBetween(0, AuthorNames.values().length);
        return AuthorNames.values()[number];
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Author: ");
        sb.append(name);
        sb.append(", age=").append(age);
        sb.append(", books=").append(books);
        return sb.toString();
    }

    public AuthorNames getName() {
        return name;
    }

    public void setName(AuthorNames name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return name == author.name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
