package by.epam.lab2019.creation;

public enum BooksNames {

    It, Game_Of_Thrones, War_and_Peace, Ulysses, Lolita, Noise_and_Rage, Invisible,
    Sweetheart, The_Bunches_Of_Wrath, Master_and_Margarita, Crime_and_Punishment,
    TheLittlePrince, Dog_sHeart, HeroOfOurTime, A_Song_Of_Ice_and_Fire

}
