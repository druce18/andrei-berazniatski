package by.epam.lab2019;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
    private static final Logger logger = Logger.getLogger(Runner.class.getName());
    private static final String DELIMITER = ": ";

    public static void main(String[] args) {

        logger.info("Point #2, year: 2018");
        LocalDate start = LocalDate.of(2018, 01, 01);
        LocalDate end = start.plusYears(1);
        List<Temperature> temperatures = Stream.iterate(start, d -> d.plusDays(1))
                .limit(ChronoUnit.DAYS.between(start, end))
                .map(Temperature::new)
                .collect(Collectors.toList());
        temperatures.forEach(temperature -> logger.info(temperature.toString()));


        logger.info("Point #3.1");
        Temperature minT = temperatures.stream()
                .min(Comparator.comparingInt(Temperature::getValue)).get();
        logger.info("Min temperature: " + minT.getValue() + " date: " + minT.getDate());

        Temperature maxT = temperatures.stream()
                .max(Comparator.comparingInt(Temperature::getValue)).get();
        logger.info("Max temperature: " + maxT.getValue() + " date: " + maxT.getDate());


        logger.info("Point #3.2");
        double result = temperatures.stream()
                .collect(Collectors.averagingInt(Temperature::getValue));
        logger.info("Average value for the year: " + Math.round(result));


        logger.info("Point #3.3");
        logger.info("Min value in each month");
        temperatures.stream()
                .collect(Collectors.groupingBy(x -> x.getDate().getMonth(),
                        Collectors.minBy(Comparator.comparingInt(Temperature::getValue))))
                .values()
                .stream()
                .map(Optional::get)
                .sorted(Comparator.comparing(Temperature::getDate))
                .forEach(t -> logger.info(t.getDate().getMonth() + DELIMITER + t.getValue()));

        logger.info("Max value in each month");
        temperatures.stream()
                .collect(Collectors.groupingBy(x -> x.getDate().getMonth(),
                        Collectors.maxBy(Comparator.comparingInt(Temperature::getValue))))
                .values()
                .stream()
                .map(Optional::get)
                .sorted(Comparator.comparing(Temperature::getDate))
                .forEach(t -> logger.info(t.getDate().getMonth() + DELIMITER + t.getValue()));


        logger.info("Point #3.4: average value in each month");
        Map<Month, Double> avgMonth = temperatures.stream()
                .collect(Collectors.groupingBy(x -> x.getDate().getMonth(),
                        Collectors.averagingInt(Temperature::getValue)));
        Stream.of(Month.values())
                .sorted(Comparator.comparing(Month::getValue))
                .forEach(month -> logger.info(month + DELIMITER + Math.round(avgMonth.get(month))));


        logger.info("Point #4");
        logger.info("Non parallel stream");
        temperatures.stream()
                .collect(new MyCollector())
                .forEach(temperature -> logger.info(temperature.getDate() + DELIMITER + temperature.getValue()));

        logger.info("Parallel stream");
        temperatures.stream()
                .parallel()
                .unordered()
                .collect(new MyCollector())
                .forEach(temperature -> logger.info(temperature.getDate() + DELIMITER + temperature.getValue()));

    }


    private static class MyCollector implements
            Collector<Temperature, Map<Month, Temperature>, List<Temperature>> {

        @Override
        public Supplier<Map<Month, Temperature>> supplier() {
            return HashMap::new;
        }

        @Override
        public BiConsumer<Map<Month, Temperature>, Temperature> accumulator() {
            return (map, temperature) -> {
                int value = temperature.getValue();
                Month month = temperature.getDate().getMonth();
                if (map.containsKey(month)) {
                    Temperature temp = map.get(month);
                    if (temp.getValue() < value) {
                        map.put(month, temperature);
                    }
                } else {
                    map.put(month, temperature);
                }
            };
        }

        @Override
        public BinaryOperator<Map<Month, Temperature>> combiner() {
            return (m1, m2) -> {
                m1.putAll(m2);
                return m1;
            };
        }

        @Override
        public Function<Map<Month, Temperature>, List<Temperature>> finisher() {
            return map -> {
                List<Temperature> list = new ArrayList<>(map.values());
                list.sort(Comparator.comparing(Temperature::getDate));
                return list;
            };
        }

        @Override
        public Set<Characteristics> characteristics() {
            return EnumSet.of(Characteristics.CONCURRENT);
        }
    }
}
