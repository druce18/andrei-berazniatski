package by.epam.lab2019;

import java.time.LocalDate;
import java.time.Month;
import java.util.Objects;

public class Temperature implements Random<LocalDate> {

    private int value;
    private LocalDate date;

    public Temperature() {
        this.value = Random.getNumberBetween(-50, 100);
        this.date = getRandom();
    }

    public Temperature(LocalDate date) {
        this.value = Random.getNumberBetween(-50, 100);
        this.date = date;
    }

    @Override
    public LocalDate getRandom() {
        int month = Random.getNumberBetween(1, 12);
        int day = Random.getNumberBetween(1, Month.of(month).maxLength());
        int year = Random.getNumberBetween(2018, 1);
        return LocalDate.of(year, month, day);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Temperature{");
        sb.append("value=").append(value);
        sb.append(", date=").append(date);
        sb.append('}');
        return sb.toString();
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Temperature that = (Temperature) o;
        return Objects.equals(date.getMonth(), that.date.getMonth());
    }

    @Override
    public int hashCode() {
        return Objects.hash(date.getMonth());
    }
}
