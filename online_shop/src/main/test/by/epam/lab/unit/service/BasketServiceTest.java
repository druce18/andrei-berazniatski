package by.epam.lab.unit.service;


import by.epam.lab.dao.ProductDAO;
import by.epam.lab.form.ProductForm;
import by.epam.lab.logs.EventLogger;
import by.epam.lab.model.Product;
import by.epam.lab.еxceptions.QuantityWrongInputException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BasketServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private ProductDAO productDAO;

    @Mock
    private EventLogger eventLogger;

    @InjectMocks
    private BasketServiceImpl basketService;


    private static final int code = 7;

    private static final String test = "test";

    private Product product = new Product(test, 1, 100);
    private Product productTest = new Product(test, 1, 9);
    private ProductForm productForm = new ProductForm(productTest);

    @Before
    public void setup() {
        basketService.cleanBasket();
        product.setCode(code);
        productTest.setCode(code);
        productForm.setCode(code);
        when(productDAO.findByCode(code)).thenReturn(product);
        basketService.addBasket(code, 9);
        basketService.addBasket(code, 9);
    }

    @Test
    public void testAddBasket() {
        List<ProductForm> productForms = new ArrayList<>();
        productForms.add(productForm);
        productForms.add(productForm);
        assertArrayEquals(productForms.toArray(), basketService.getBasket().toArray());
    }

    @Test(expected = QuantityWrongInputException.class)
    public void testAddBasketFail() {
        basketService.addBasket(code, -9);
        basketService.addBasket(code, null);
    }

    @Test
    public void testCleanBasket() {
        basketService.cleanBasket();
        assertEquals(0, basketService.getBasket().size());
    }

    @Test
    public void deleteProductBasket() {
        basketService.deleteProductBasket(code);
        assertEquals(0, basketService.getBasket().size());
    }
}
