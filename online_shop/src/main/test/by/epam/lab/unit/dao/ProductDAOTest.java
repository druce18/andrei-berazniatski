package by.epam.lab.unit.dao;

import by.epam.lab.config.AppConfig;
import by.epam.lab.dao.ProductDAO;
import by.epam.lab.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class ProductDAOTest {

    @Autowired
    private ProductDAO productDAO;

    private static final int code = 5;

    @Test
    public void testFindByCode() {
        Product product = productDAO.findByCode(code);
        assertEquals(code, product.getCode());
    }

    @Test
    public void testUpdate() {
        Product product = productDAO.findByCode(code);
        product.setQuantityBuy(1);
        productDAO.update(product);
    }

    @Test
    public void testGetAll() {
        List<Product> products = productDAO.getAll();
        assertNotNull(products);
    }

}
