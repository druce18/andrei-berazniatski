package by.epam.lab.unit.service;

import by.epam.lab.dao.ProductDAO;
import by.epam.lab.form.ProductForm;
import by.epam.lab.logs.EventLogger;
import by.epam.lab.model.Product;
import by.epam.lab.validation.ProductValidator;
import by.epam.lab.еxceptions.ProductWrongInputException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private ProductDAO productDAO;

    @Mock
    private EventLogger eventLogger;

    @Mock
    private ProductValidator productValidator;

    @InjectMocks
    private ProductServiceImpl productService;

    private static final int code = 7;
    private static final String test = "test";
    private ProductForm productForm = new ProductForm(7, test, 1, 9);
    private Product product = new Product(test, 1, 9);

    @Test
    public void testFindByCode() {
        when(productDAO.findByCode(code)).thenReturn(product);
        Product product = productService.findByCode(code);
        assertNotNull(product);
        assertEquals(test, product.getName());
        verify(productDAO, times(1)).findByCode(code);
    }

    @Test
    public void testFindByCodeFail() {
        when(productDAO.findByCode(code)).thenReturn(null);
        Product product = productService.findByCode(code);
        assertNull(product);
        verify(productDAO, times(1)).findByCode(code);
    }

    @Test
    public void testSave() {
        when(productValidator.checkProduct(productForm)).thenReturn(true);
        productService.save(productForm);
        verify(productValidator, times(1)).checkProduct(productForm);
    }

    @Test(expected = ProductWrongInputException.class)
    public void testSaveFail() {
        when(productValidator.checkProduct(any())).thenReturn(false);
        productService.save(productForm);
        verify(productValidator, times(1)).checkProduct(productForm);
    }

    @Test
    public void testGetAll() {
        when(productDAO.getAll()).thenReturn(new ArrayList<>());
        List<Product> products = productService.getAll();
        assertNotNull(products);
        verify(productDAO, times(1)).getAll();
    }

    @Test
    public void testGetAllFail() {
        when(productDAO.getAll()).thenReturn(null);
        List<Product> products = productService.getAll();
        assertNull(products);
        verify(productDAO, times(1)).getAll();
    }

    @Test
    public void testDelete() {
        when(productDAO.delete(code)).thenReturn(product);
        Product product = productService.delete(code);
        assertNotNull(product);
        verify(productDAO, times(1)).delete(code);
    }

    @Test
    public void testDeleteFail() {
        when(productDAO.delete(code)).thenReturn(null);
        Product product = productService.delete(code);
        assertNull(product);
        verify(productDAO, times(1)).delete(code);
    }

    @Test
    public void testUpdate() {
        when(productValidator.checkProduct(any())).thenReturn(true);
        productService.update(productForm);
        verify(productValidator, times(1)).checkProduct(any());
    }

    @Test(expected = ProductWrongInputException.class)
    public void testUpdateFail() {
        when(productValidator.checkProduct(any())).thenReturn(false);
        productService.update(productForm);
        verify(productValidator, times(1)).checkProduct(any());
    }
}