package by.epam.lab.unit.service;

import by.epam.lab.Constants;
import by.epam.lab.dao.UserDAO;
import by.epam.lab.logs.EventLogger;
import by.epam.lab.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    private EventLogger eventLogger;

    @Autowired
    public UserServiceImpl(UserDAO userDAO, EventLogger eventLogger) {
        this.userDAO = userDAO;
        this.eventLogger = eventLogger;
    }

    @Override
    public boolean save(User user) {
        boolean flag = false;
        if (user.getUsername() != null && user.getUsername().length() > 0 && user.getPassword() != null
                && user.getPassword().length() > 0 && userDAO.findByUsername(user.getUsername()) == null) {
            userDAO.save(user);
            eventLogger.logEvent(user.getUsername() + Constants.REGISTERED);
            flag = true;
        }
        return flag;
    }

    @Override
    public User findByUsername(String username) {
        return userDAO.findByUsername(username);
    }

    @Override
    public String usernameNow() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}