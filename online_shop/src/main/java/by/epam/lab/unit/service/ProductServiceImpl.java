package by.epam.lab.unit.service;

import by.epam.lab.Constants;
import by.epam.lab.dao.ProductDAO;
import by.epam.lab.form.ProductForm;
import by.epam.lab.logs.EventLogger;
import by.epam.lab.model.Product;
import by.epam.lab.validation.ProductValidator;
import by.epam.lab.еxceptions.ProductWrongInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private UserService userService;

    private ProductDAO productDAO;

    private EventLogger eventLogger;

    private ProductValidator productValidator;

    @Autowired
    public ProductServiceImpl(UserService userService, ProductDAO productDAO, EventLogger eventLogger, ProductValidator productValidator) {
        this.userService = userService;
        this.productDAO = productDAO;
        this.eventLogger = eventLogger;
        this.productValidator = productValidator;
    }

    @Override
    public Product findByCode(int code) {
        return productDAO.findByCode(code);
    }

    @Override
    public void save(ProductForm product) {
        if (productValidator.checkProduct(product)) {
            productDAO.save(product.getProduct());
            eventLogger.logEvent(userService.usernameNow() + Constants.SAVE + product);
        } else {
            throw new ProductWrongInputException();
        }
    }

    @Override
    public List<Product> getAll() {
        return productDAO.getAll();
    }

    @Override
    public Product delete(int code) {
        Product product = productDAO.delete(code);
        eventLogger.logEvent(userService.usernameNow() + Constants.DELETE + product);
        return product;
    }

    @Override
    public void update(ProductForm product) {
        if (productValidator.checkProduct(product)) {
            productDAO.update(product.getProduct());
            eventLogger.logEvent(userService.usernameNow() + Constants.UPDATE + product);
        } else {
            throw new ProductWrongInputException();
        }
    }

}
