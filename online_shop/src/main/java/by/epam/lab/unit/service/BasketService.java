package by.epam.lab.unit.service;

import by.epam.lab.form.ProductForm;

import java.util.List;

public interface BasketService {

    void addBasket(int code, Integer count);

    List<ProductForm> getBasket();

    List<ProductForm> buyProducts();

    List<ProductForm> cleanBasket();

    void deleteProductBasket(int code);

}
