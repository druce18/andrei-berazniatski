package by.epam.lab.unit.service;

import by.epam.lab.form.ProductForm;
import by.epam.lab.model.Product;

import java.util.List;

public interface ProductService {

    Product findByCode(int code);

    void save(ProductForm product);

    List<Product> getAll();

    Product delete(int code);

    void update(ProductForm product);

}
