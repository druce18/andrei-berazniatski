package by.epam.lab.unit.service;

import by.epam.lab.model.User;

public interface UserService {

    boolean save(User user);

    User findByUsername(String username);

    String usernameNow();

}
