package by.epam.lab.dao;

import by.epam.lab.model.Product;

import java.util.List;

public interface ProductDAO {

    Product findByCode(int code);

    void save(Product product);

    List<Product> getAll();

    Product delete(int code);

    void update(Product product);
}
