package by.epam.lab.dao;

import by.epam.lab.model.User;


public interface UserDAO {

    User findByUsername(String username);

    void save(User user);

}
