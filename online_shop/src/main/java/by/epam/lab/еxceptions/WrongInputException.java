package by.epam.lab.еxceptions;

import by.epam.lab.Constants;

public class WrongInputException extends RuntimeException {

    public WrongInputException() {
        this(Constants.WRONG_INPUT);
    }

    public WrongInputException(String message) {
        super(message);
    }
}
