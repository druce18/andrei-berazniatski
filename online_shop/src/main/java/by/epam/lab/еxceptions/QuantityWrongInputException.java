package by.epam.lab.еxceptions;

import by.epam.lab.Constants;

public class QuantityWrongInputException extends WrongInputException {

    public QuantityWrongInputException() {
        this(Constants.WRONG_INPUT_QUANTITY);
    }

    public QuantityWrongInputException(String message) {
        super(message);
    }
}
