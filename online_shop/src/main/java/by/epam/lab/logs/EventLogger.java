package by.epam.lab.logs;

public interface EventLogger {

    void logEvent(String msg);

}
