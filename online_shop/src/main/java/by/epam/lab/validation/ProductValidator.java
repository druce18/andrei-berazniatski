package by.epam.lab.validation;

import by.epam.lab.form.ProductForm;

public interface ProductValidator {

    boolean checkProduct(ProductForm product);

}
