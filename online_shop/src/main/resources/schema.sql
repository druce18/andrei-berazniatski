create table users
(
    id       int auto_increment,
    username varchar(45)                     not null,
    password varchar(45)                     not null,
    role     varchar(45) default 'ROLE_USER' not null,
    constraint id_UNIQUE
        unique (id),
    constraint username_UNIQUE
        unique (username)
);

alter table users
    add primary key (id);

INSERT INTO users
VALUES (1, 'admin', 'admin', 'ROLE_ADMIN');
INSERT INTO users
VALUES (2, 'user', 'user', 'ROLE_USER');


create table products
(
    code     int auto_increment,
    name     varchar(45) not null,
    price    int         not null,
    quantity int         not null,
    image    longblob    not null,
    constraint code_UNIQUE
        unique (code)
);

alter table products
    add primary key (code);

INSERT INTO products
VALUES (1, 'coffee', 10, 100);
INSERT INTO shop.products
VALUES (2, 'tea', 5, 225);