package by.epam.lab2019;

import org.junit.Test;

public class ValidationTest {

    @Test
    public void testValidateInt() throws ValidationFailedException {
        ValidationSystem.validate(1);
        ValidationSystem.validate(5);
        ValidationSystem.validate(10);
        ValidationSystem.validate(15);
        ValidationSystem.validate(25);
        ValidationSystem.validate(30);
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateIntFails() throws ValidationFailedException {
        ValidationSystem.validate(-18);
        ValidationSystem.validate(0);
        ValidationSystem.validate(11);
        ValidationSystem.validate(14);
        ValidationSystem.validate(31);
        ValidationSystem.validate(100);
    }

    @Test
    public void testValidateString() throws ValidationFailedException {
        ValidationSystem.validate("Andrey, OK");
        ValidationSystem.validate("Number 12345678");
        ValidationSystem.validate("Hello, it's me");
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateStringFails() throws ValidationFailedException {
        ValidationSystem.validate("andrey, OK");
        ValidationSystem.validate("Number 123456789");
        ValidationSystem.validate("let's go!");
        ValidationSystem.validate("hello, can you hear me? ");
    }

    @Test
    public void testValidatePerson() throws ValidationFailedException {
        ValidationSystem.validate(new Person(17, 161));
        ValidationSystem.validate(new Person(22, 182));
        ValidationSystem.validate(new Person(20, 165));
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidatePersonFails() throws ValidationFailedException {
        ValidationSystem.validate(new Person(16, 160));
        ValidationSystem.validate(new Person(16, 170));
        ValidationSystem.validate(new Person(17, 160));
        ValidationSystem.validate(new Person(11, 140));
        ValidationSystem.validate(new Person(14, 155));
    }

    @Test
    public void testValidateObject() throws ValidationFailedException {
        Object obj1 = 7;
        ValidationSystem.validate(obj1);
        Object obj2 = "Hi!!!";
        ValidationSystem.validate(obj2);
        Object obj3 = new Person(17, 161);
        ValidationSystem.validate(obj3);
    }

    @Test(expected = ValidationFailedException.class)
    public void testValidateObjectFails() throws ValidationFailedException {
        Float obj1 = new Float(1);
        ValidationSystem.validate(obj1);
        Object obj2 = new Double(3);
        ValidationSystem.validate(obj2);
        Object obj3 = null;
        ValidationSystem.validate(obj3);
        Object obj4 = new Person(16, 160);
        ValidationSystem.validate(obj4);
        Object obj5 = "hello, can you hear me? ";
        ValidationSystem.validate(obj5);
    }

}
