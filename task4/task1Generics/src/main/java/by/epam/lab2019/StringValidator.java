package by.epam.lab2019;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator implements Validator<String> {

    @Override
    public boolean check(String text) {
        boolean mainFlag = false;
        int flag1 = -1;
        int flag2 = -1;

        Pattern pattern = Pattern.compile("^[A-Z]");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            flag1 = matcher.start();
        }

        pattern = Pattern.compile("$");
        matcher = pattern.matcher(text);
        while (matcher.find()) {
            flag2 = matcher.start();
        }

        if ((flag1 == 0) && (flag2 <= 15)) {
            mainFlag = true;
        }

        return mainFlag;
    }


}
