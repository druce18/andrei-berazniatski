package by.epam.lab2019;

public class ValidationSystem {

    public static void validate(Integer number) throws ValidationFailedException {
        Validator validator = new IntegerValidator();
        if (!validator.check(number)) {
            throw new ValidationFailedException();
        }
    }

    public static void validate(String text) throws ValidationFailedException {
        Validator validator = new StringValidator();
        if (!validator.check(text)) {
            throw new ValidationFailedException();
        }
    }

    public static void validate(Person person) throws ValidationFailedException {
        Validator validator = new PersonValidator();
        if (!validator.check(person)) {
            throw new ValidationFailedException();
        }
    }

    public static void validate(Object o) throws ValidationFailedException {
        if (o == null) {
            throw new ValidationFailedException();
        }
        if (o.getClass() == String.class) {
            validate((String) o);
        } else if (o.getClass() == Integer.class) {
            validate((Integer) o);
        } else if (o.getClass() == Person.class) {
            validate((Person) o);
        } else {
            throw new ValidationFailedException();
        }
    }
}
