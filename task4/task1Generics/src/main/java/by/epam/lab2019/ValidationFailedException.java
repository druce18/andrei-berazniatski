package by.epam.lab2019;

import javax.validation.ValidationException;

public class ValidationFailedException extends ValidationException {
    private static final String VALIDATION_FAILED_EXCEPTION = "validation failed";

    public ValidationFailedException() {
        super(VALIDATION_FAILED_EXCEPTION);
    }

}
