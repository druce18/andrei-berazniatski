package by.epam.lab2019;

public class PersonValidator implements Validator<Person> {

    @Override
    public boolean check(Person person) {
        boolean flag = false;
        if (person.getAge() > 16 && person.getHeight() > 160) {
            flag = true;
        }
        return flag;
    }

}
