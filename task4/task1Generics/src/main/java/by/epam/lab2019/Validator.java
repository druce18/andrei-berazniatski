package by.epam.lab2019;

public interface Validator<T> {

    boolean check(T element);

}
