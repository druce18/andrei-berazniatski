package by.epam.lab2019;

public class IntegerValidator implements Validator<Integer> {

    @Override
    public boolean check(Integer element) {
        boolean flag = false;
        int number = element;
        if ((number >= 1 && number <= 10) || (number >= 15 && number <= 30)) {
            flag = true;
        }
        return flag;
    }

}
