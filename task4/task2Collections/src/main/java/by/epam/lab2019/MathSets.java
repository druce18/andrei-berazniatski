package by.epam.lab2019;

import java.util.HashSet;
import java.util.Iterator;

public class MathSets<E> {

    public HashSet<E> unionSets(HashSet<E> setA, HashSet<E> setB) {
        HashSet<E> setC = new HashSet<>(setA);
        Iterator<E> iterator = setB.iterator();
        while (iterator.hasNext()) {
            setC.add(iterator.next());
        }
        return setC;
    }

    public HashSet<E> intersectionSets(HashSet<E> setA, HashSet<E> setB) {
        HashSet<E> setC = new HashSet<>();
        Iterator<E> iterator = setB.iterator();
        while (iterator.hasNext()) {
            E buffer = iterator.next();
            if (setA.contains(buffer)) {
                setC.add(buffer);
            }
        }
        return setC;
    }

    public HashSet<E> differenceSets(HashSet<E> setA, HashSet<E> setB) {
        HashSet<E> setC = new HashSet<>(setA);
        Iterator<E> iterator = setB.iterator();
        while (iterator.hasNext()) {
            setC.remove(iterator.next());
        }
        return setC;
    }

    public HashSet<E> symmetricDifferenceSets(HashSet<E> setA, HashSet<E> setB) {
        HashSet<E> setC = new HashSet<>();
        setC.addAll(differenceSets(setA, setB));
        setC.addAll(differenceSets(setB, setA));
        return setC;
    }

}
