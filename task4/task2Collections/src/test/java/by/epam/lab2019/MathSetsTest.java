package by.epam.lab2019;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.HashSet;

public class MathSetsTest {

    HashSet<Integer> setA;
    HashSet<Integer> setB;
    HashSet<Integer> setC;
    MathSets<Integer> mathSets = new MathSets<>();

    @Test
    public void testUnionSets() {
        setA = new HashSet<>();
        setB = new HashSet<>();
        setC = new HashSet<>();

        setA.add(1);
        setA.add(2);
        setA.add(4);

        setB.add(3);
        setB.add(4);
        setB.add(5);
        setB.add(6);

        setC.add(1);
        setC.add(2);
        setC.add(3);
        setC.add(4);
        setC.add(5);
        setC.add(6);

        assertEquals(setC, mathSets.unionSets(setA, setB));
    }

    @Test
    public void testIntersectionSets() {
        setA = new HashSet<>();
        setB = new HashSet<>();
        setC = new HashSet<>();

        setA.add(1);
        setA.add(2);
        setA.add(4);

        setB.add(3);
        setB.add(4);
        setB.add(5);
        setB.add(2);

        setC.add(2);
        setC.add(4);

        assertEquals(setC, mathSets.intersectionSets(setA, setB));
    }

    @Test
    public void testDifferenceSets() {
        setA = new HashSet<>();
        setB = new HashSet<>();
        setC = new HashSet<>();

        setA.add(1);
        setA.add(2);
        setA.add(3);
        setA.add(4);

        setB.add(3);
        setB.add(4);
        setB.add(5);

        setC.add(1);
        setC.add(2);

        assertEquals(setC, mathSets.differenceSets(setA, setB));
    }

    @Test
    public void testSymmetricDifferenceSets() {
        setA = new HashSet<>();
        setB = new HashSet<>();
        setC = new HashSet<>();

        setA.add(1);
        setA.add(2);
        setA.add(4);
        setA.add(3);

        setB.add(3);
        setB.add(4);
        setB.add(5);
        setB.add(6);

        setC.add(1);
        setC.add(2);
        setC.add(5);
        setC.add(6);

        assertEquals(setC, mathSets.symmetricDifferenceSets(setA, setB));
    }
}
